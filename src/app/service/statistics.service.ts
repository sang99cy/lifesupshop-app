import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {apiUrl} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class StatisticsService {

  constructor(private http: HttpClient) {
  }

  thongkeDoanhThuHangNgay(time: any): Observable<any> {
    return this.http.post(`${apiUrl}/statistics/sumOrder/createFromTo`, time)
  }

  thongkeSoluongDonHangHomnay(): Observable<any> {
    return this.http.get(`${apiUrl}/statistics/countOrder/currentDate`).pipe();
  }

  thongkeSoluongDonHangThangnay(): Observable<any> {
    return this.http.get(`${apiUrl}/statistics/countOrder/currentMonth`).pipe();
  }

  thongkeDoanhThuHomnay(): Observable<any> {
    return this.http.get(`${apiUrl}/statistics/sumOrder/currentDate`).pipe();
  }

  thongkeDoanhThuThangnay(): Observable<any> {
    return this.http.get(`${apiUrl}/statistics/sumOrder/currentMonth`).pipe();
  }

  thongkeDoanhThuOrSoluongTheoQuy(type: number): Observable<any> {
    return this.http.get(`${apiUrl}/statistics/sumCountOrder/quarter?type=${type}`).pipe();
  }


}
