import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { apiUrl } from '../../environments/environment';
import { CookieService } from 'ngx-cookie-service';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { UserService } from './user.service';
import { Cart } from '../model/Cart';
import { Item } from '../model/Item';
import { JwtResponse } from '../response/JwtResponse';
import { ProductInOrder } from '../model/ProductInOrder';
import { ToastrService } from 'ngx-toastr';
import { OrderCheckout } from '../model/OrderCheckout';
import { ProductInfo } from '../model/productInfo';

@Injectable({
  providedIn: 'root',
})
export class CartService {
  private cartUrl = `${apiUrl}/cart`;
  items_: Item[] = [];
  localMap: any = {};

  itemsSubject: BehaviorSubject<Item[] | null>;
  private totalSubject: BehaviorSubject<number | null>;
  public items: Observable<Item[] | null>;
  public total: Observable<number | null>;

  private currentUser?: JwtResponse;

  constructor(
    private http: HttpClient,
    private cookieService: CookieService,
    private toastr: ToastrService,
    private userService: UserService
  ) {
    this.itemsSubject = new BehaviorSubject<Item[] | null>(null);
    this.items = this.itemsSubject.asObservable();
    this.totalSubject = new BehaviorSubject<number | null>(null);
    this.total = this.totalSubject.asObservable();
    // this.userService.currentUser?.subscribe(user => {this.currentUser = user);
    //get local product

    this.items_ = this.parseProductInCart();
    this.itemsSubject.next(this.items_);
  }

  private getLocalCart(): ProductInOrder[] {
    let cartList = localStorage.getItem('cart');
    let cartListParse = cartList ? JSON.parse(cartList) : [];
    return cartListParse;
  }
  parseProductInCart() {
    let productsList = this.getLocalCart();
    let new_productList = [];
    for (var product_id in productsList) {
      var item: Item = {
        productInfo: productsList[product_id],
        quantity: productsList[product_id]['count'],
      };
      new_productList.push(item);
    }
    return new_productList;
  }
  getCart(): Observable<ProductInOrder[]> {
    const localCart = this.getLocalCart();
    if (this.currentUser) {
      if (localCart.length > 0) {
        return this.http.post<Cart>(this.cartUrl, localCart).pipe(
          tap((_) => {
            this.clearLocalCart();
          }),
          map((cart: any) => cart.products),
          catchError((_) => of([]))
        );
      } else {
        return this.http.get<Cart>(this.cartUrl).pipe(
          map((cart: any) => cart.products),
          catchError((_) => of([]))
        );
      }
    } else {
      return of(localCart);
    }
  }

  addItem(productInOrder: ProductInOrder): Observable<boolean> {
    this.items_.push({
      productInfo: productInOrder,
      quantity: productInOrder.count,
    });
    this.itemsSubject.next(this.items_);

    if (!this.currentUser) {
      let cartList = localStorage.getItem('cart');

      if (cartList) {
        let cartListParse = cartList ? JSON.parse(cartList) : {};
        this.localMap = cartListParse;
      }
      if (!(this.localMap as any)[productInOrder.productId]) {
        (this.localMap as any)[productInOrder.productId] = productInOrder;
        console.log(this.localMap);
      } else {
        (this.localMap as any)[productInOrder.productId].count +=
          productInOrder.count;
      }
      localStorage.setItem('cart', JSON.stringify(this.localMap));
      return of(true);
    } else {
      const url = `${this.cartUrl}/add`;
      return this.http.post<boolean>(url, {
        quantity: productInOrder.count,
        productId: productInOrder.productId,
      });
    }
  }
  //
  //
  update(productInOrder: any): Observable<ProductInOrder | null> {
    if (this.currentUser) {
      const url = `${this.cartUrl}/${productInOrder.productId}`;
      return this.http.put<ProductInOrder>(url, productInOrder.count);
    }
    return of(null);
  }

  remove(productInOrder: ProductInOrder): Observable<any> {
    if (!this.currentUser) {
      delete this.localMap[productInOrder.productId];
      return of(null);
    } else {
      const url = `${this.cartUrl}/${productInOrder.productId}`;
      return this.http.delete(url).pipe();
    }
  }

  checkout(checkoutBody: OrderCheckout): Observable<any> {
    const url = `${this.cartUrl}/checkout`;
    return this.http.post(url, checkoutBody).pipe();
  }

  storeLocalCart() {
    this.cookieService.set('cart', JSON.stringify(this.localMap));
  }

  storeLocalCart1() {
    this.cookieService.set('favorite', JSON.stringify(this.localMap));
  }

  clearLocalCart() {
    console.log('clear local cart');
    this.cookieService.delete('cart');
    this.localMap = {};
  }

  cartPlusOrMinus(productId: string, action: string, quantity: number = 0) {
    var productIndex = this.items_.findIndex(
      (product) => product.productInfo.productId == productId
    );
    if (action == 'change') {
      this.items_[productIndex].quantity = quantity;
    } else {
      var quantityPlusOrMinus = action == 'plus' ? quantity : -quantity;
      this.items_[productIndex].quantity += quantityPlusOrMinus;
    }
    this.updateToLocalStorage()
    this.itemsSubject.next(this.items_);
  }

  removeItemFromCart(productId:string){
    var productIndex = this.items_.findIndex(
      (product) => product.productInfo.productId == productId
    );
    this.items_.splice(productIndex, 1);
    this.updateToLocalStorage()
    this.itemsSubject.next(this.items_)
  }

  updateToLocalStorage(){
    var productDict:any = {}
    for (var item of this.items_){
      item.productInfo.count = item.quantity
      productDict[item.productInfo.productId] = item.productInfo
    }
    localStorage.setItem("cart", JSON.stringify(productDict))

  }

  get payTotal(){
    var paytotal =0
    for(var item of this.items_){
      paytotal+=item.productInfo.productPrice*item.quantity
    }
    return paytotal
  }
}
