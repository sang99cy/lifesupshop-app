import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {apiUrl} from '../../environments/environment';
import {BehaviorSubject, Observable, of, Subject} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {JwtResponse} from '../response/JwtResponse';
import {User} from "../model/User";
import {CookieService} from "ngx-cookie-service";
import {Router} from "@angular/router";
import {JsonPipe} from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userUrl = `${apiUrl}/userList`;
  currentUserSubject = new BehaviorSubject<any>(null);
  public currentUser: Observable<JwtResponse | null>
  public nameTerms = new Subject<string>();
  public name$ = this.nameTerms.asObservable();

  constructor(private http: HttpClient,
              private router: Router,
              private cookieService: CookieService) {

    let user_ = localStorage.getItem('currentUser')
    let user_parse = user_ ? <JwtResponse>JSON.parse(user_) : null

    this.currentUserSubject.next(user_parse)

    this.currentUser = this.currentUserSubject.asObservable();
    cookieService.set('currentUser', <string>localStorage.getItem('currentUser'));
  }

  set User(user: any) {

    this.currentUserSubject.next(user);
    localStorage.setItem("currentUser", JSON.stringify(user))

  }

  get currentUserValue() {
    return this.currentUserSubject.value;
  }


  login(loginForm: any): Observable<any> {
    const url = `${apiUrl}/login`;
    return this.http.post<any>(url, loginForm).pipe(
      map(user => {

        if (user && user.token) {

          //this.nameTerms.next(user.name);
          this.User = user;

          return user;
        }
      }),
      catchError(this.handleError('Login Failed', null))
    );
  }

  logout() {
    this.currentUserSubject.next(null);
    localStorage.removeItem('currentUser');
    localStorage.removeItem('token');
    this.cookieService.delete('currentUser');
    this.router.navigateByUrl('/shop')
  }

  signUp(user: User): Observable<User> {
    const url = `${apiUrl}/register`;
    return this.http.post<User>(url, user);
  }

  update(user: User): Observable<User> {
    const url = `${apiUrl}/profile`;
    return this.http.put<User>(url, user);
  }

  get(email: string): Observable<User> {
    const url = `${apiUrl}/profile/${email}`;
    return this.http.get<User>(url);
  }


  getAllInPage(page: number, size: number): Observable<any> {
    const url = `${this.userUrl}?page=${page}&size=${size}`;
    return this.http.get(url)
      .pipe(
        // tap(_ => console.log(_)),
      )
  }

  getAllUsers(): Observable<any> {
    return this.http.get(`${apiUrl}/users`);
  }

  delete(id: any): Observable<any> {
    const url = `${apiUrl}/user/delete/${id}`;
    return this.http.delete<any>(url);
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.log(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  changingPassword(userId: string, oldPass: string, newPass: string): Observable<any> {
    let uri = `${apiUrl}/profile/changePassword?userId=${userId}&newPassword=${newPass}&oldPassword=${oldPass}`
    return this.http.put(uri, null)
  }

  mapToFormData(body: any): FormData {
    const formData = new FormData();
    for (const property in body) {
      if (body.hasOwnProperty(property)) {
        formData.append(property, body[property]);
      }
    }
    return formData;
  }

  uploadImage(image: File, userId: number) {

    let url = `${apiUrl}/uploadAvatar?userId=${userId}`

    //let formData = this.mapToFormData({...user,"image":image})
    let formData = this.mapToFormData({image: image})
    return this.http.post(url, formData)
  }
}
