import {Injectable} from '@angular/core';
import {apiUrl} from "../../environments/environment";
import {BehaviorSubject, Observable, of, Subject} from "rxjs";
import {JwtResponse} from "../response/JwtResponse";
import {HttpClient} from "@angular/common/http";
import {CookieService} from "ngx-cookie-service";
import {catchError, tap} from "rxjs/operators";
import {User} from "../model/User";

@Injectable({
  providedIn: 'root'
})
export class TestService {

  private userUrl = `${apiUrl}/userList`;
  private userUrl1 = `${apiUrl}/delete/user`;
  private currentUserSubject: BehaviorSubject<JwtResponse | null>;
  public currentUser: Observable<JwtResponse | null> | undefined;
  public nameTerms = new Subject<string>();
  public name$ = this.nameTerms.asObservable();

  constructor(private http: HttpClient,
              private cookieService: CookieService) {
    this.currentUserSubject = new BehaviorSubject<JwtResponse | null>(JSON.parse(<string>localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
    cookieService.set('currentUser', <string>localStorage.getItem('currentUser'));
  }

  get currentUserValue() {
    return this.currentUserSubject.value;
  }


  login(loginForm: any): Observable<JwtResponse | null> {
    const url = `${apiUrl}/login`;
    return this.http.post<any>(url, loginForm).pipe(
      tap(user => {
        console.log(JSON.stringify(user));
        if (user && user.token) {
          this.cookieService.set('currentUser', JSON.stringify(user));
          if (loginForm.remembered) {
            localStorage.setItem('currentUser', JSON.stringify(user));
          }
          console.log((user.name));
          this.nameTerms.next(user.name);
          this.currentUserSubject.next(user);
          return user;
        }
      }),
      catchError(this.handleError('Login Failed', null))
    );
  }

  logout() {
    this.currentUserSubject.next(null);
    localStorage.removeItem('currentUser');
    this.cookieService.delete('currentUser');
  }

  signUp(user: User): Observable<User> {
    const url = `${apiUrl}/register`;
    return this.http.post<User>(url, user);
  }

  update(user: User): Observable<User> {
    const url = `${apiUrl}/profile`;
    return this.http.put<User>(url, user);
  }

  get(email: string): Observable<User> {
    const url = `${apiUrl}/profile/${email}`;
    return this.http.get<User>(url);
  }

  getAllInPage(page: number, size: number): Observable<any> {
    const url = `${this.userUrl}?page=${page}&size=${size}`;
    return this.http.get(url)
      .pipe(
        // tap(_ => console.log(_)),
      )
  }

  delete(id: User): Observable<any> {
    return this.http.delete<any>(`${this.userUrl1}/${id}`);
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.log(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
