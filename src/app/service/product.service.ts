import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {ProductInfo} from '../model/productInfo';
import {apiUrl} from '../../environments/environment';
import {ToastrService} from 'ngx-toastr';
import {AdminProduct} from '../admin/product-management/ admin-product';
import is from "@sindresorhus/is";
import any = is.any;

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  productCurrentName$ = new BehaviorSubject<any>({type:"search",key:null})
  categoryCurrentType$ = new BehaviorSubject<any>(null)

  set productCurrentName(searchtype:any) {
    this.productCurrentName$.next(searchtype);
  }

  set categoryCurrentType(categoryType: number){
    console.log('category of sang: ',categoryType)
    this.categoryCurrentType$.next(categoryType)
  }



  private productUrl = `${apiUrl}/product`;
  private categoryUrl = `${apiUrl}/category`;
  private baseurl = `${apiUrl}/delete/product`;
  private productUrl1 = `${apiUrl}/product/under25`;
  private productUrl2 = `${apiUrl}/product/from25to50`;
  private productUrl3 = `${apiUrl}/product/from50to100`;
  private productUrl4 = `${apiUrl}/product/from100to200`;
  private productUrl5 = `${apiUrl}/product/above200`;

  constructor(private http: HttpClient,
              private toastr: ToastrService,) {

  }

  public options = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      Authorization: 'Basic ',// your authorization method
      'Access-Control-Allow-Headers':
        'Origin, X-Requested-With, Content-Type, Accept'
    })
  };

  getProductFilter(productName: any = null): Observable<any> {
    let uri = `${apiUrl}/product/dosearch`
    let searchBody = {productName: productName}
    return this.http.post(uri, searchBody)
  }

  getAllInPage(page: number, size: number): Observable<any> {
    const url = `${this.productUrl}?page=${page}&size=${size}`;
    return this.http.get(url)
      .pipe(
        // tap(_ => console.log(_)),
      )
  }

  createProduct1(product: ProductInfo): Observable<any> {
    return this.http.post<any>(`${apiUrl}/seller/product/new`, product);
  }

  getCategoryInPage(categoryType: number, page: number, size: number): Observable<any> {
    const url = `${this.categoryUrl}/${categoryType}?page=${page}&size=${size}`;
    return this.http.get(url).pipe(
      // tap(data => console.log(data))
    );
  }

  getunder25(page = 1, size = 10): Observable<any> {
    return this.http.get(`${this.productUrl1}?page=${page}&size=${size}`).pipe();
  }

  getfrom25to50(page = 1, size = 10): Observable<any> {
    return this.http.get(`${this.productUrl2}?page=${page}&size=${size}`).pipe();
  }

  getfrom50to100(page = 1, size = 10): Observable<any> {
    return this.http.get(`${this.productUrl3}?page=${page}&size=${size}`).pipe();
  }

  getfrom100to200(page = 1, size = 10): Observable<any> {
    return this.http.get(`${this.productUrl4}?page=${page}&size=${size}`).pipe();
  }

  getabove200(page = 1, size = 10): Observable<any> {
    return this.http.get(`${this.productUrl5}?page=${page}&size=${size}`).pipe();
  }

  getDetail(id: String): Observable<ProductInfo> {
    const url = `${this.productUrl}/${id}`;
    return this.http.get<ProductInfo>(url).pipe(
      catchError(_ => {
        console.log("Get Detail Failed");
        return of(new ProductInfo());
      })
    );
  }

  update(productInfo: ProductInfo): Observable<ProductInfo> {
    const url = `${apiUrl}/seller/product/${productInfo.productId}/edit`;
    return this.http.put<ProductInfo>(url, productInfo);
  }

  create(productInfo: AdminProduct): Observable<any> {
    const url = `${apiUrl}/seller/product/new`;
    return this.http.post<any>(url, productInfo);
  }


  deleteProduct1(productId: string): Observable<any> {
    return this.http.delete<any>(`${this.baseurl}/${productId}`);
  }

  getAllProduct(): Observable<any> {
    const url = `${apiUrl}/products`;
    return this.http.get(url)
      .pipe(
        // tap(_ => console.log(_)),
      )
  }

  getALlProductHaveSearch(keySearch: any, page: any, size: any): Observable<any> {
    console.log('page:', page, 'size', size)
    let url = `${apiUrl}/products/page?page=${page}&size=${size}`;
    if (keySearch != null) {
      page = 0;
      size = 8;
      url = `${apiUrl}/products/page?page=${page}&size=${size}`;
      return this.http.post(url, {keySearch: keySearch},this.options);
    }
    return this.http.post(url, {keySearch: keySearch},this.options);
  }


  changeStatus(productId: string, status: number): Observable<any> {
    const url = `${this.productUrl}/status?productId=${productId}&status=${status}`;
    return this.http.post(url, null).pipe();
  }

  // addFavouriteProduct(productInfo: ProductInfo) {
  //
  //   const a: ProductInfo[] = JSON.parse(localStorage.getItem("avf_item")) || [];
  //   a.push(productInfo);
  //   this.toastr.success("Adding Product", "Adding Product as Favourite");
  //   setTimeout(() => {
  //     localStorage.setItem("avf_item", JSON.stringify(a));
  //   }, 1500);
  //
  // }
  //
  // getLocalFavouriteProducts(): ProductInfo[] {
  //   const productInfo: ProductInfo[] =
  //     JSON.parse(localStorage.getItem("avf_item")) || [];
  //
  //   return productInfo;
  // }
  newProductUpload(product: any): Observable<any> {
    console.log("loa loa", product);
    let url = `${apiUrl}/products/newUpload`;


    //return of(null)
    return this.http.post(url, product);
  }

  updateProductUpload(product: any): Observable<any> {
    let url = `${apiUrl}/products/editUpload`;


    //return of(null)
    return this.http.post(url, product);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error); // log to console instead


      return of(result as T);
    };
  }

  mapToFormData(body: any): FormData {
    const formData = new FormData();
    for (const property in body) {
      if (body.hasOwnProperty(property)) {
        formData.append(property, body[property]);
      }
    }
    return formData;
  }

  uploadImage(file: any) {

    let formData = this.mapToFormData({file: file})
    let url = `${apiUrl}/products/uploadImage`
    return this.http.post(url, formData)
  }


  getAllProductByCategory(categoryType: number, page: number, size: number): Observable<any> {
    const url = `${apiUrl}/products/page?page=${page}&size=${size}&categoryType=${categoryType}`;
    return this.http.get(url);
  }


}
