import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/internal/Observable";
import {Category} from "../model/Category";
import {apiUrl} from "../../environments/environment";
import {catchError} from "rxjs/operators";
import {of} from "rxjs";
import {AdminCategory} from "../admin/category-management/admin-category";

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  private categoryUrl = `${apiUrl}/categoryList`;
  private deleteCategoryUrl = `${apiUrl}/delete/category`;
  private detailCategoryUrl = `${apiUrl}/category`;

  //private deleteCategoryUrl=`${apiUrl}//category/delete`;

  constructor(private http: HttpClient) {
  }

  getAll(): Observable<any> {
    let url = `${apiUrl}/categories`
    return this.http.get(url);
  }

  getPage(page = 1, size = 10): Observable<any> {
    return this.http.get(`${this.categoryUrl}?page=${page}&size=${size}`).pipe();
  }

  update(category: AdminCategory): Observable<AdminCategory> {
    const url = `${apiUrl}/seller/category/${category.categoryId}/edit`;
    return this.http.put<Category>(url, category);
  }

  create(category: AdminCategory): Observable<AdminCategory> {
    const url = `${apiUrl}/seller/category/new`;
    return this.http.post<AdminCategory>(url, category);
  }

  deleteCategory(categoryId: Category): Observable<any> {
    return this.http.delete<any>(`${this.deleteCategoryUrl}/${categoryId}`);
  }

  // createProduct1(category: Category): Observable<any> {
  //   return this.http.post<any>(`${apiUrl}/seller/category/new`, category);
  // }

  getDetailcategoy(id: string): Observable<any> {
    const url = `${this.detailCategoryUrl}/${id}`;
    return this.http.get<Category>(url).pipe(
      catchError(_ => {
        console.log("Get Detail Failed");
        return of(new Category());
      })
    );
  }

  changeStatus(categoryId: number, status: number): Observable<any> {
    const url = `${apiUrl}/category/status?categoryId=${categoryId}&status=${status}`;
    return this.http.post(url, null).pipe();
  }

  categoriesByCategoryType(status: number, categoryType: number): Observable<any> {
    const url = `${apiUrl}/categories/aonam?status=${status}&categoryType=${categoryType}`;
    return this.http.get(url).pipe(
      catchError(_ => {
        console.log("Get category ao nam Failed");
        return of(new Category());
      }))
  }




}
