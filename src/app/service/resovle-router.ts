import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router} from '@angular/router';
import {UserService} from './user.service';

@Injectable()
export class AccountResolverService implements Resolve<null> {
  constructor(private router: Router, private accountService: UserService) {}

  public resolve(route: ActivatedRouteSnapshot, router: any) {
    this.accountService.currentUser.subscribe(res => {
      if (!res) {
          this.router.navigate(['/login'])
        }
        /*} else if (res.authorities.every((authority: any) => !route.data?.role?.includes(authority.name))) {
            this.router.navigate(['pages/not-permission']);*/
    });

    return null
    // const language = route.paramMap.get('lang');
    //
    // if(allowedLanguages.includes(language)) {
    //   return null;
    // } else {
    //   this.router.navigate(['404']);
    // }
  }
}
