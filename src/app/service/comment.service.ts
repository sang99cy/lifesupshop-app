import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {apiUrl} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private http: HttpClient) {
  }

  getAllProduct(): Observable<any> {
    let url = `${apiUrl}/comments/all`;
    return this.http.get(url)
  }

  newComment(comment: any): Observable<any> {
    let url = `${apiUrl}/comments`;
    return this.http.post(url, comment)
  }

  viewCommentByProduct(prductId: string): Observable<any> {
    let url = `${apiUrl}/comments?productId=${prductId}`;
    return this.http.get(url)
  }

  editComment(comment: any): Observable<any> {
    let url = `${apiUrl}/comments`;
    return this.http.put(url, comment)
  }

  deleteComment(commentId: number): Observable<any> {
    let url = `${apiUrl}/comments?commentId=${commentId}`;
    return this.http.delete(url)
  }

  duyetComment(commentId: number, status: number): Observable<any> {
    let url = `${apiUrl}/comments/status?commentId=${commentId}&status=${status}`;
    return this.http.post(url, null)
  }
}
