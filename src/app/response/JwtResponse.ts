export class JwtResponse {
  token?: string;
  type?: string;
  account?: string;
  name?: string;
  address?: string;
  phone?: string;
  role?: string;
  avatar?: string;

  // constructor(token: string, type: string, account: string, name: string, address: string, phone: string, role: string) {
  //   this.token = token;
  //   this.type = type;
  //   this.account = account;
  //   this.name = name;
  //   this.address = address;
  //   this.phone = phone;
  //   this.role = role;
  // }

  constructor(token: string, type: string, account: string, name: string, address: string, phone: string, role: string, avatar: string) {
    this.token = token;
    this.type = type;
    this.account = account;
    this.name = name;
    this.address = address;
    this.phone = phone;
    this.role = role;
    this.avatar = avatar;
  }
}

