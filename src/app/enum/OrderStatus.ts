/*export enum OrderStatus {
    "Pending", /!* chờ xử xử lý*!/
    "Finished", /!* đã hoàn thành*!/
    "Cenceled", /!* đã bị hủy*!/
    "Approved"  /!* đã được duyệt*!/
}*/
export enum OrderStatus {
  "chờ xác nhận", /* chờ xử xử lý*/
  "Hoàn thành", /* đã hoàn thành*/
  "Hủy đơn hàng", /* đã bị hủy*/
  "Đang xử lý"  /* đã được duyệt*/
}
