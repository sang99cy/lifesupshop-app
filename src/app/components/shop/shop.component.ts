import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable, of, Subscription } from 'rxjs';
import { Product } from 'src/app/model/product';
import { HttpProdService } from 'src/app/service/http-prod.service';
import { HttpServerService } from '../../service/http-server.service';
import { ProductService } from '../../service/product.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../service/user.service';
import { CookieService } from 'ngx-cookie-service';
import { ToastrService } from 'ngx-toastr';
import { CartService } from '../../service/cart.service';
import { ProductInOrder } from '../../model/ProductInOrder';
import { ProductInfo } from '../../model/productInfo';
import { MatTableDataSource } from '@angular/material/table';
import { AdminProduct } from '../../admin/product-management/ admin-product';
import { PageEvent } from '@angular/material/paginator';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css'],
})
export class ShopComponent implements OnInit {
  search_key: any = null;
  category_id: any = null;
  title?: string;
  pagesale?: any;
  private paramSub?: Subscription;
  private querySub?: Subscription;
  loading = false;
  selectedBrand = 'All';
  status = 'all';
  productsList: any;
  productStatus = 'getAllproduct';
  productInfo?: ProductInfo;
  count: number = 0;
  dataSource = new MatTableDataSource<AdminProduct>();
  public pageSlice: any;
  /*code phan trang*/
  totalElements: number = 0;
  products: Object[] = [];
  page: any = 0;
  size: any = 8;
  request: any;
  filter_via_name: boolean = true;
  constructor(
    private productService: ProductService,
    private route: ActivatedRoute,
    private cartService: CartService,
    private toastr: ToastrService,
    private cookieService: CookieService,
    private userService: UserService,
    private router: Router
  ) {}

  @ViewChild('paginator') paginator: any;

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit() {
    this.userService.currentUserSubject.subscribe((user) => {
      //console.log("use shop", user)
    });

    // this.productService.categoryCurrentType$.subscribe((categoryType:number)=>{
    //   //console.log("danh muc cua toi: ", categoryType)
    //   this.getAllProductByCategory(categoryType,this.page,this.size);
    // })
    //console.log('categoryId:',this.route.snapshot.paramMap.get('id'));
    /*filete theo danh muc*/
    /*this.route.params.subscribe((params) => {
      this.category_id = params['id'];
      if (this.category_id) {
        this.filter_via_name = false;
        this.getAllProductByCategory(this.category_id, this.page, this.size);
      }




    });*/

    this.productService.productCurrentName$.subscribe((typeSearch:any) => {
      //console.log("search productName", productName)

      if(typeSearch.type == "search"){
      this.search_key = typeSearch.key;

      this.getProductFilter(this.search_key, this.page, this.size);}
      else{
        this.getAllProductByCategory(typeSearch.key,this.page,this.size)
      }
    });
  }

  getAllProductByCategory(categoryType: number, page: number, size: number) {
    this.productService
      .getAllProductByCategory(categoryType, page, size)
      .subscribe((res) => {
        console.log(res);
        this.productsList = res['content'];
        this.totalElements = res['totalElements'];
      });
  }

  getAllProduct() {
    this.productService.getAllProduct().subscribe((res) => {
      this.productsList = res;
    });
  }

  getProductFilter(productName: any, page: any, size: any) {
    console.log('search productName', productName);
    // this.productService.getProductFilter(productName).subscribe(res =>{
    //
    //   this.productsList = res;
    // })

    /*code doi*/
    let pageProduct = this.page;
    let sizeProduct = this.size;
    console.log(page, '/', size);
    this.productService
      .getALlProductHaveSearch(productName, page, size)
      .subscribe((res) => {
        this.productsList = res['content'];
        console.log('content[]: ', res['content']);
        this.totalElements = res['totalElements'];
        console.log('totalElements: ', this.totalElements);
      });
  }

  OnPageChange(event: PageEvent) {
    this.page = event.pageIndex.toString();
    this.size = event.pageSize.toString();

    if (!this.filter_via_name) {
      this.getAllProductByCategory(this.category_id, this.page, this.size);
    }
    //this.getProductFilter(null, this.page, this.size)

    //console.log("search productName", productName)
    else this.getProductFilter(this.search_key, this.page, this.size);
  }

  ngOnDestroy(): void {
    this.querySub?.unsubscribe();
    this.paramSub?.unsubscribe();
  }

  update() {
    if (this.route.snapshot.queryParamMap.get('pages')) {
      // const currentPage = +this.route.snapshot.queryParamMap.get('pages');
      // const size = +this.route.snapshot.queryParamMap.get('size');
      // this.getProds(currentPage, size);
      this.getProds(1, 50);
    } else {
      this.getProds();
    }
  }

  getProds(page: number = 1, size: number = 12) {
    if (this.route.snapshot.url.length == 1) {
      this.productService.getAllInPage(+page, +size).subscribe((page) => {
        this.pagesale = page;
        this.title = 'Get Whatever You Want!';
      });
    } else {
      //  /category/:id
      const type = this.route.snapshot.url[1].path;
      this.productService
        .getCategoryInPage(+type, page, size)
        .subscribe((categoryPage) => {
          this.title = categoryPage.category;
          this.pagesale = categoryPage.page;
        });
    }
  }

  addToCart(product: ProductInOrder, count: number = 1) {
    this.cartService
      .addItem(<ProductInOrder>{ ...product, count: count })
      .subscribe(
        (res) => {
          if (!res) {
            console.log('Add Cart failed' + res);
            throw new Error();
          }
          this.toastr.success('Add cart success!', 'Wow checkout now!');
          this.router.navigateByUrl('/cart');
        },
        (_) => console.log('Add Cart Failed')
      );
  }

  // addToCart(productInfo: any) {
  //   // this.cartService
  //   //   .addItem(new ProductInOrder(productInfo))
  //   //   .subscribe(
  //   //     res => {
  //   //       if (!res) {
  //   //         console.log('Add Cart failed' + res);
  //   //         throw new Error();
  //   //       }
  //   //       this.toastr.success('Add cart success!', 'Wow checkout now!');
  //   //       this.router.navigateByUrl('/cart');
  //   //     },
  //   //     _ => console.log('Add Cart Failed')
  //   //   );
  // }

  addToFavorite(productInfo: any) {
    // this.productService.addFavouriteProduct(productInfo);
  }

  // setLang(lang: string) {
  //   // console.log("Language", lang);
  //   this.translate.use(lang).then(() => {
  //   });
  // }

  ShowProduct(productPrice: number) {
    const getAll = this.productStatus === 'getAllproduct';
    const getunder25 = this.productStatus === 'under25' && 25 >= productPrice;
    const get25To50 =
      this.productStatus === '2550' && 50 > productPrice && productPrice >= 25;
    const get50To100 =
      this.productStatus === '50100' &&
      100 > productPrice &&
      productPrice >= 50;
    const get100To200 =
      this.productStatus === '100200' &&
      200 > productPrice &&
      productPrice >= 100;
    const get200above =
      this.productStatus === '200above' && productPrice >= 200;
    return (
      getAll ||
      getunder25 ||
      get25To50 ||
      get50To100 ||
      get100To200 ||
      get200above
    );
  }

  buyNow(product: ProductInOrder) {
    this.addToCart(product);
  }
}
