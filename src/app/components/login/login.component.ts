import {Component, Inject, InjectionToken, OnInit} from '@angular/core';
import {UserService} from "../../service/user.service";
import {ActivatedRoute, Route, Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";
import {Role} from "../../enum/Role";
import {HttpServerService} from "../../service/http-server.service";
import {TestService} from "../../service/test.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [

    ToastrService,
    UserService
  ]
})
export class LoginComponent implements OnInit {

  isInvalid?: boolean;
  isLogout?: boolean;
  submitted = false;
  model: any = {
    username: '',
    password: '',
    remembered: false
  };

  returnUrl: string | null = '/';

  constructor(public userService: UserService,
              private router: Router,
              private toastr: ToastrService,
              private  route: ActivatedRoute){}

  ngOnInit(): void {
    let params = this.route.snapshot.queryParamMap;
    this.isLogout = params.has('logout');
    this.returnUrl = params.get('returnUrl');
    if(this.returnUrl){
      this.toastr.error("Bạn không có quyền",undefined,{positionClass:'toast-top-right'})
    }
  }

  onSubmit() {
    this.submitted = true;
    this.userService.login(this.model).subscribe(
      (user:any) => {

        if (user) {
          if (user.role == Role.Manager) {
            window.location.href = "/admin/dashboard"

          }
          else if (user.role == Role.Employee) {
            this.returnUrl = '/admin/products';
          }else {
            window.location.href = "/"
            this.returnUrl = '/';
          }
          //alert('Authentication Success! Logged in!');
          this.toastr.success('Authentication Success!', 'Logged in!');
          this.router.navigateByUrl(this.returnUrl || '{}');
        }
        else {
          this.toastr.error('Authentication Error!', 'Logged in!');
          this.isLogout = false;
          this.isInvalid = true;
        }
      },
      error => { alert('Authentication Error !')}
    );
  }

  fillLoginFields(u: any, p : any) {
    this.model.username = u;
    this.model.password = p;
    this.onSubmit();
  }
}
