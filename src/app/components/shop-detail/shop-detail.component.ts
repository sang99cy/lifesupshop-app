import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { Product } from 'src/app/model/product';
import { HttpProdService } from '../../service/http-prod.service';
import { HttpServerService } from '../../service/http-server.service';
import { ProductInOrder } from '../../model/ProductInOrder';
import { ProductInfo } from '../../model/productInfo';
import { ProductService } from '../../service/product.service';
import { CartService } from '../../service/cart.service';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../service/user.service';
import { CookieService } from 'ngx-cookie-service';
import { CommentService } from 'src/app/service/comment.service';
import { map } from 'rxjs/operators';
import {apiUrl} from "../../../environments/environment";
import {data} from "../../admin/product-management/data";

@Component({
  selector: 'app-shop-detail',
  templateUrl: './shop-detail.component.html',
  styleUrls: ['./shop-detail.component.css'],
})
export class ShopDetailComponent implements OnInit {
  userComment: string = '';
  commnetListObservable = new Observable<any>();
  user: any;
  productId: string = '';
  title: string = '';
  count: number = 0;
  productInfo: ProductInfo = new ProductInfo();
  userId: any = null;
  // ProductStatus = ProductStatus;

  constructor(
    private productService: ProductService,
    private cartService: CartService,
    private cookieService: CookieService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private userService: UserService,
    private router: Router,
    private commentSevice: CommentService
  ) {}

  ngOnInit() {
    //get userinfor
    this.userService.currentUserSubject.subscribe((user) => {
      this.user = user;
    });
    //get product ids
    this.route.params.subscribe((param) => {
      this.productId = param['id'];
      //get all comment
      this.commnetListObservable = this.getCommentList(this.productId);
    });
    this.getProduct();
    this.title = 'Product Detail';
    this.count = 1;
    this.getListCommentSse()
  }

  getProduct(): void {
    const id = this.route.snapshot.paramMap.get('id');
    var id_ = id ? id : '';
    this.productService.getDetail(id_).subscribe(
      (prod) => {
        console.log('product', prod);
        this.productInfo = prod;
      },
      (_) => console.log('Get Cart Failed')
    );
  }

  addToCart() {
    this.cartService
      .addItem(<ProductInOrder>{ ...this.productInfo, count: this.count })
      .subscribe(
        (res) => {
          if (!res) {
            console.log('Add Cart failed' + res);
            throw new Error();
          }
          this.toastr.success('Add cart success!', 'Wow checkout now!');
          this.router.navigateByUrl('/cart');
        },
        (_) => console.log('Add Cart Failed')
      );
  }

  validateCount() {
    console.log('Validate');
    const max = this.productInfo?.productStock;
    var max_ = max == undefined ? 0 : max;
    if (this.count > max_) {
      this.count = max_;
    } else if (this.count < 1) {
      this.count = 1;
    }
  }

  comment() {
    console.log(this.userId)
    if(this.user != null){
      let commentBodyPost = {
        comment: this.userComment,
        productId: this.productId,
        userId: this.user.userId,
      };

      this.commentSevice.newComment(commentBodyPost).subscribe(res=>{
        this.commnetListObservable = this.getCommentList(this.productId)
      });
    }else {
      this.toastr.warning('hãy đăng nhập tài khoản để bình luận', 'bình luận')
    }
  }

  getCommentList(productId: string) {
    console.log('ss',productId)
    return this.commentSevice.viewCommentByProduct(productId).pipe(
      map((res) => {
        console.log('all commetn', res);
        return res;
      })
    );
  }

  getListCommentSse(){
    let source = new EventSource(`${apiUrl}/comments/subscribe/`);
    /*duyet binh luan*/
    source.addEventListener('status', (message) => {
      let res = JSON.parse(message.data);
      if(data) {
        //window.location.reload();
        this.commnetListObservable = this.getCommentList(this.productId);
      }
    });
    source.addEventListener('end', function (event) {
      //console.log('Handling end....');
      source.close();
    });
  }
}
