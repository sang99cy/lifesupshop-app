import {Component, OnInit} from '@angular/core';
import {HttpProdService} from 'src/app/service/http-prod.service';
import {UserService} from "../../service/user.service";
import {FormBuilder, FormControl, FormControlName, FormGroup, Validator, Validators} from "@angular/forms";
import {CartService} from "../../service/cart.service";
import {OrderCheckout} from "../../model/OrderCheckout";
import {ProductInOrder} from "../../model/ProductInOrder";
import {ToastrService} from 'ngx-toastr';
import {Router} from "@angular/router";
import { PaymentType } from 'src/app/enum/payment-type';
import {OrderStatus} from "../../enum/OrderStatus";

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  countTotal: number = 0;
  priceTotal: number = 0;
  productsInCart: any[] = []
  formGroup: FormGroup;
  formGroupAttributes = {
    name: new FormControl("", Validators.required),
    address: new FormControl("", Validators.required),
    phone: new FormControl("", Validators.required),
    account: new FormControl("", Validators.required),
    comment: new FormControl("", Validators.required),
    payment: new FormControl("", Validators.required),
  }
  map: {id: number; name: string}[] = [];
  dataLocal: any
  userInfor: any
  user: any

  constructor(private infProd: HttpProdService,
              private authService: UserService,
              private formBuilder: FormBuilder,
              private cartService: CartService,
              private toastr: ToastrService,
              private router: Router
  ) {
    this.formGroup = this.formBuilder.group(this.formGroupAttributes)
  }
  ngOnInit(): void {
    this.cartService.getCart().subscribe((res: any) => {
      for (const k in res) {
        this.countTotal += res[k].count
        this.priceTotal += res[k].productPrice * res[k].count
        this.productsInCart.push(res[k])
      }

    })
    this.getUserInfor()
    const productLocal = localStorage.getItem('cartLocal')
    if (productLocal) {
      const a = JSON.parse(productLocal)
      console.log('aaaa', a);

      this.dataLocal = a

    }
    this.infProd.product_Cart.subscribe((data) => {
      console.log('datacheckout', data);
      this.dataLocal = data
    });
    this.converTNumToArray()
    this.user = this.authService.currentUserValue
  }

  converTNumToArray(){
    for(var n in PaymentType) {
      if (typeof PaymentType[n] === 'number') {
        this.map.push({id: <any>OrderStatus[n], name: n});
      }
    }
    console.log(this.map);
  }

  getUserInfor() {
    this.userInfor = this.authService.currentUserValue
    this.formGroup.patchValue(this.userInfor)
  }

  get FormValue() {
    return this.formGroup.value
  }


  onCheckout() {
    console.log(this.user)
    if(this.user == null){
      this.toastr.warning('Bạn cần đăng nhập để thực hiện thanh toán', 'Thanh toán!')
    }
    let orderBody: OrderCheckout = {

      buyerEmail: this.FormValue.account,
      buyerName: this.FormValue.name,
      buyerPhone: this.FormValue.phone,
      buyerAddress: this.FormValue.address,
      orderAmount: this.priceTotal,
      orderStatus: 0,
      userId: this.userInfor.userId,
      payment: this.FormValue.payment,
      comment: this.FormValue.comment,
      detailOrders: this.productsInCart
    }
    //this.router.navigateByUrl(`/order-details/${157}`)
    this.cartService.checkout(orderBody).subscribe(res => {

      this.toastr.success('Đang xử lý thanh toán', 'Thanh toán  Thành thông!')
      //remove all product in local Strorage
      localStorage.removeItem("cart")
     //this.router.navigateByUrl(`/order-details/${157}`)
      this.redirectToShop()
    })
  }

  redirectToShop() {
    this.router.navigateByUrl("/shop")
  }
}
