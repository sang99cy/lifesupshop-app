import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { OrderService } from 'src/app/service/order.service';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css'],
})
export class OrderDetailsComponent implements OnInit {
  orderId: number = 0;
  OrderInfor: any;
  OrderDetailsList: any = [];
  constructor(
    private orderService: OrderService,

    private activateRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    //get order id
    this.activateRoute.params.subscribe((params) => {
      this.orderId = params['orderId'];
      this.orderService.getOrderByOrderId(this.orderId).subscribe((res) => {
        this.OrderInfor = res;
        console.log(this.OrderInfor);
      });
      this.orderService
        .getOrderDetailsByOrdersId(this.orderId)
        .subscribe((res) => {
          this.OrderDetailsList = res;
          console.log(this.OrderDetailsList)
        });
    });
  }
}
