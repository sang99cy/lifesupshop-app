import {Component, OnInit, ViewChild} from '@angular/core';

import {CategoryService} from '../../service/category.service';
import {UserService} from 'src/app/service/user.service';
import {FormControl} from '@angular/forms';
import {Observable, of} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {MatAutocompleteTrigger} from '@angular/material/autocomplete';
import {ProductService} from 'src/app/service/product.service';
import {ActivatedRoute, Router} from "@angular/router";
import {CartService} from 'src/app/service/cart.service';
import {ProductInOrder} from 'src/app/model/ProductInOrder';
import {Item} from 'src/app/model/Item';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  user: any;
  categories: any;
  categoryType: any;
  categoryAoNam: any;
  categoryQuanNam: any;
  categoryPhuKien: any;
  categoryDayDep: any;
  items: Item[] = [];

  constructor(
    private categoryService: CategoryService,
    private userService: UserService,
    private productService: ProductService,
    private router: ActivatedRoute,
    private cartService: CartService,
    private router_:Router
  ) {
  }

  @ViewChild(MatAutocompleteTrigger) autocomplete!: MatAutocompleteTrigger;

  ngOnInit(): void {
    //get cart
    this.cartService.itemsSubject.subscribe(items => {
      this.items = items ? items : this.items

    })
    this.getListCategory();
    this.getCategoriesBycategoryType(0, 0)
    this.userService.currentUserSubject.subscribe((user: any) => {
      console.log("user now", user)
      this.user = user
    })

    this.filteredOptions = this.productService.getAllProduct().pipe(map(products => {
      console.log("all producs", products)
      return products
    }))


    this.autocomplete.closePanel();
  }


  getListCategory() {
    this.categoryService.getAll().subscribe((res) => {
      console.log(res);
      this.categories = res;
    });
  }

  logoutUser() {
    this.userService.logout();
  }

  myControl = new FormControl();
  options: string[] = ['One', 'Two', 'Three'];
  filteredOptions: Observable<any> = of();


  getProductName(productName: string) {
    this.productService.productCurrentName = {type:"search",key:productName}
    let url =  window.location.href
    let componentName = url.split("/")[3]
    if(componentName!="shop"){
      this.router_.navigateByUrl("/shop")
    }

  }

  getDataByCategory(categoryId: number) {
    this.productService.categoryCurrentType = categoryId;
  }

  getCategoriesBycategoryType(status: number, categoryType: number) {
    /*danh muc ao nam*/
    this.categoryService.categoriesByCategoryType(0, 0).subscribe(res => {
      //console.log('ao nam: ', res)
      this.categoryAoNam = res;
    });
    /*danh muc quan*/
    this.categoryService.categoriesByCategoryType(0, 1).subscribe(res => {
      //console.log('quan nam: ', res)
      this.categoryQuanNam = res
    });
    /*danh muc phu kien*/
    this.categoryService.categoriesByCategoryType(0, 2).subscribe(res => {
      //console.log('quan nam: ', res)
      this.categoryPhuKien = res
    });
    /*danh muc phu kien*/
    this.categoryService.categoriesByCategoryType(0, 3).subscribe(res => {
      //console.log('quan nam: ', res)
      this.categoryDayDep = res
    });
  }

  searchByType(cat_id:number){
    this.productService.productCurrentName = {type:"category",key:cat_id}
  }

  ChangeQuantity(productId: string, action: string, quantity: any = 1) {

    this.cartService.cartPlusOrMinus(productId, action, Number(quantity))
  }

  removeItemFromCart(productId: string) {
    this.cartService.removeItemFromCart(productId)
  }

}
