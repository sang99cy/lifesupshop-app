import {Component, OnInit} from '@angular/core';
import {User} from "../../model/User";
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";
import {UserService} from "../../service/user.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user: User;

  constructor(
    private userService: UserService,
    private router: Router,
    private toastr: ToastrService) {
    this.user = new User();

  }

  ngOnInit() {


  }

  onSubmit() {
    this.userService.signUp(this.user).subscribe(u => {
        this.toastr.success('Signup Success', 'wellcome!!');
        this.router.navigate(['/login']);
      },
      e => {
      });
  }
}
