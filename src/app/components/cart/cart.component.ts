import {AfterContentChecked, Component, OnDestroy, OnInit} from '@angular/core';
import {ProductInOrder} from "../../model/ProductInOrder";
import {debounceTime, switchMap} from "rxjs/operators";
import {Subject, Subscription} from "rxjs";
import {JwtResponse} from "../../response/JwtResponse";
import {CartService} from "../../service/cart.service";
import {ToastrService} from "ngx-toastr";
import {Router} from "@angular/router";
import {UserService} from "../../service/user.service";
import {Role} from "../../enum/Role";
import {OrderService} from "../../service/order.service";
import { Item } from 'src/app/model/Item';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],

})
export class CartComponent implements OnInit, OnDestroy {

  items:Item[] = []
  constructor(private cartService: CartService,
              private orderService: OrderService,
              private userService: UserService,
              public toastr: ToastrService,
              private router: Router
  ) {
    this.userSubscription = this.userService.currentUser.subscribe(user => this.currentUser = user);
  }

  productInOrders :any= [];
  total:number = 1;
  currentUser: any;
  name$:any;
  name: string = '';
  userSubscription: Subscription;

  private  updateTerms = new Subject<ProductInOrder>();
  sub: Subscription = new Subscription();

  static validateCount(productInOrder:any) {
    const max = productInOrder.productStock;
    if (productInOrder.count > max) {
      productInOrder.count = max;
    } else if (productInOrder.count < 1) {
      productInOrder.count = 1;
    }
    console.log(productInOrder.count);
  }

  ngOnInit() {
    this.name$ = this.userService.name$.subscribe(aName => this.name = aName);
    this.cartService.itemsSubject.subscribe((items:any)=>{

      this.items = items? items:this.items
       this.total = this.cartService.payTotal

    })

    this.sub = this.updateTerms.pipe(
      debounceTime(300),
      switchMap((productInOrder: ProductInOrder) => this.cartService.update(productInOrder))
    ).subscribe(prod => {
        if (prod) { throw new Error(); }
      },
      _ => console.log('Update Item Failed'));
  }

  ngOnDestroy() {
    if (!this.currentUser) {
      this.cartService.storeLocalCart();
    }
    this.userSubscription.unsubscribe();
  }

  // ngAfterContentChecked() {
  //   this.total = this.productInOrders.reduce(
  //     (prev:any, cur:any) => prev + cur.count * cur.productPrice, 0);
  // }

  addOne(productInOrder:any) {
    const max = productInOrder.productStock
    this.cartService.cartPlusOrMinus(productInOrder.productId,"plus",1)
    const count = productInOrder.count++;
    if(count >= max){
      this.toastr.warning('Số lượng sản phẩm tối đa bạn có thể mua là:' + max)
    }
    CartComponent.validateCount(productInOrder);
    if (this.currentUser) { this.updateTerms.next(productInOrder); }
  }

  minusOne(productInOrder:any) {
    this.cartService.cartPlusOrMinus(productInOrder.productId,"minus",1)
    const count = productInOrder.count--;
    CartComponent.validateCount(productInOrder);
    if (this.currentUser) { this.updateTerms.next(productInOrder); }
    console.log(count)
    if(count <= 0){
      console.log('sdfascaret')
      this.remove(productInOrder);
    }
  }

  onChange(productInOrder:any) {
    this.cartService.cartPlusOrMinus(productInOrder.productId,"change",productInOrder.count)
    CartComponent.validateCount(productInOrder);
    if (this.currentUser) { this.updateTerms.next(productInOrder); }
  }


  remove(productInOrder: ProductInOrder) {
    this.cartService.removeItemFromCart(productInOrder.productId)
    console.log('remove:',productInOrder)
    this.cartService.remove(productInOrder).subscribe(
      (success : any) => {
        this.productInOrders = this.productInOrders.filter((e:any) => e.productId !== productInOrder.productId);
        localStorage.removeItem('cart')
        console.log('Cart: ' + this.productInOrders);
        this.toastr.warning('Remove cart success!', 'Continue shopping!');
      },
      _ => console.log('Remove Cart Failed'));
  }

  checkout() {
    // if (!this.currentUser) {
    //   this.router.navigate(['/login'], {queryParams: {returnUrl: this.router.url}});
    // }
    // // else if (this.currentUser.role !== Role.Customer) {
    // //   this.toastr.warning('Please login account to checkout', 'Checkout Cart!!');
    // //   this.router.navigate(['/shop']);
    // // }
    // else {
    //   //check cart in local storage
    //   let cartInfor_ = localStorage.getItem("cart")
    //   let cartInfor = cartInfor_? JSON.parse(cartInfor_) : {}
    //   if(Object.keys(cartInfor).length > 0)
    //   //ìf exist then save to product order
    //
    //   {
    //     this.orderService.addProductToOrder(this.productInOrders).subscribe(res=>{
    //       console.log(res);
    //     })
    //     //remove cart from localstroge
    //   }
    //   this.cartService.checkout().subscribe(
    //     _ => {
    //       this.productInOrders = [];
    //       this.toastr.success('Waiting for approval', 'Checkout Cart Success!!');
    //     },
    //     error1 => {
    //       console.log('Checkout Cart Failed');
    //       this.toastr.success('Checkout Cart Failed', 'Checkout Cart Failed!!');
    //     });
    //   this.router.navigate(['/']);
    // }

  }
  /*setLang(lang: string) {
    this.translate.use(lang).then(() => {});
  }*/

  redirectToShop() {
    this.router.navigateByUrl("/shop")

  }
}
