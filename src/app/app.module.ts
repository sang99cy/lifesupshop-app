import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ModuleadminModule } from './admin/moduleadmin.module';
import {NbStatusService} from "@nebular/theme";
import { HomeclientComponent } from './homeclient/homeclient.component';
import { RouterModule } from '@angular/router';
import { HeaderComponent } from './components/header/header.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { FooterComponent } from './components/footer/footer.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { ContactComponent } from './components/contact/contact.component';
import { CartComponent } from './components/cart/cart.component';
import { ShopComponent } from './components/shop/shop.component';
import { ShopDetailComponent } from './components/shop-detail/shop-detail.component';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {NbMenuModule} from "@nebular/theme";
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import {CookieService} from "ngx-cookie-service";
import { ToastrModule } from 'ngx-toastr';
import { NavigationComponent } from './components/navigation/navigation.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatPaginatorModule} from "@angular/material/paginator";
import { NgHttpLoaderModule } from 'ng-http-loader';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { UserProfileModule } from './shared/modules/user-profile.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { MatInputModule } from '@angular/material/input';
import { ImagePipe } from './admin/Image.pipe';
import { CommonModule } from '@angular/common';
import { OrderDetailsComponent } from './components/order-details/order-details.component';

@NgModule({
  declarations: [
    HeaderComponent,
    CategoriesComponent,
    HomeclientComponent,
    FooterComponent,
    CheckoutComponent,
    ContactComponent,
    CartComponent,
    ShopComponent,
    ShopDetailComponent,
    AppComponent,
    LoginComponent,
    RegisterComponent,
    NavigationComponent,
    UserProfileComponent,
    ImagePipe,
    OrderDetailsComponent
  ],
  imports: [
    MatInputModule,
    MatAutocompleteModule,
    MatFormFieldModule,
    UserProfileModule,
    ModuleadminModule,
    AppRoutingModule,
    BrowserModule,
    CommonModule,
    RouterModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NbMenuModule.forRoot(),
    ToastrModule.forRoot({
      positionClass :'toast-top-right'
    }),
    MatPaginatorModule,
    BrowserAnimationsModule,
    NgHttpLoaderModule.forRoot()
  ],
  providers: [NbStatusService,CookieService],
  bootstrap: [AppComponent],
})
export class AppModule {}
