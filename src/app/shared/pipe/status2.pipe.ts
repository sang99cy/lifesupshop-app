import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'status2'
})
export class StatusPipe2 implements PipeTransform {

  transform(value: number) {
    switch(value) {
      case 0:
        return "Chờ xác nhận"
      case 1:
        return "Giao hàng"
      case 2:
        return "Hủy đơn hàng"
      default:
        return "Hoàn thành"
    }
  }

}
