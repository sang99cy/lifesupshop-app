import { NgModule } from '@angular/core';
import { ProfilePictureComponent } from '../components/profile-picture/profile-picture.component';
import { UserInforComponent } from '../components/user-infor/user-infor.component';
import { UserProfileComponent } from '../components/user-profile/user-profile.component';
import { ImageModule } from './cropper-image.module';
import { MatTabsModule } from '@angular/material/tabs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import { AvatarPipe } from '../components/user-infor/avatar.pipe';
import {StatusPipe} from "../../admin/order-management/status.pipe";
import {StatusPipe2} from "../pipe/status2.pipe";

@NgModule({
  imports: [
    ImageModule,
    MatTabsModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
  ],
  declarations: [
    ProfilePictureComponent,
    UserInforComponent,
    UserProfileComponent,
    AvatarPipe,
    StatusPipe2
  ],
  exports: [ProfilePictureComponent, UserInforComponent, UserProfileComponent],
})
export class UserProfileModule {}
