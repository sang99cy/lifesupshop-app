import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ImageCropperModule } from 'ngx-image-cropper';
import { CropperImageComponent } from '../components/cropper-image/cropper-image.component';


@NgModule({
    imports: [  ImageCropperModule,FormsModule,CommonModule ],
    declarations: [ CropperImageComponent ],
    exports: [ CropperImageComponent]
})
export class ImageModule { }
