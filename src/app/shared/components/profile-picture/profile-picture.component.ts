import {Component, Input, OnInit} from '@angular/core';
import {UserService} from "../../../service/user.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-profile-picture',
  templateUrl: './profile-picture.component.html',
  styleUrls: ['./profile-picture.component.scss']
})
export class ProfilePictureComponent implements OnInit {
  @Input("avatar") avatarPic: string = 'src/assets/img/avatar.png'
  @Input("user") user: any = 0

  constructor(
    private userService: UserService,
    private toarts: ToastrService) {
  }

  ngOnInit(): void {

  }

  getImgLink(event: any) {

    this.userService.uploadImage(event.file,this.user.userId).subscribe((res:any)=>{

      //update user profile
      let new_user = {...this.user,avatar:res.avatar}

      this.userService.User =  new_user
      alert("Cập nhật thành công")
      this.toarts.success("cập nhật ảnh đại diện thành công","ảnh đại diện")
    })

  }


}
