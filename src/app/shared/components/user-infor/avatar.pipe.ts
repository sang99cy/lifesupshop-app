import { Pipe, PipeTransform } from '@angular/core';
import { apiUrl } from 'src/environments/environment';

@Pipe({
  name: 'avatar'
})
export class AvatarPipe implements PipeTransform {

  transform(value: string) {
    return `${apiUrl}/${value}`
  }

}
