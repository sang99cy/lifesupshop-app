import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import { UserService } from 'src/app/service/user.service';
import {MatTableDataSource} from "@angular/material/table";
import {AdminOrder} from "../../../admin/order-management/admin-order";
import {OrderService} from "../../../service/order.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-user-infor',
  templateUrl: './user-infor.component.html',
  styleUrls: ['./user-infor.component.scss'],
})
export class UserInforComponent implements OnInit,AfterViewInit {
  displayedColumns: string[] = ["orderId", "buyerPhone", "buyerAddress", "createTime", "orderStatus", "orderAmount","action"];
  dataSource = new MatTableDataSource<AdminOrder>();
  @ViewChild('paginator') paginator: any;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }


  user: any;
  old_pass: string = '';
  new_pass: string = '';
  new_pass_confirm: string = '';

  constructor(
    private userService: UserService,
    private orderService: OrderService,
    private toastr: ToastrService,
  ) {}

  ngOnInit(): void {
    this.userService.currentUserSubject.subscribe((user) => {

    this.user = user

    });
    /*do don hang thoe userId*/
    // this.orderService.getAllOrders().subscribe(res => {
    //   console.log(res);
    //   this.dataSource.data = res
    // })
    this.orderService.getOrderByUserId(this.user.userId).subscribe(res => {
         console.log(res);
          this.dataSource.data = res
    })
  }

  change() {
    if (this.new_pass_confirm == this.new_pass) {
      this.userService.changingPassword(this.user.userId,this.old_pass,this.new_pass).subscribe(res => {
        if(res != null){
          this.toastr.success("Thay đổi mật khẩu thành công, Mật khẩu")
        }
      })

    } else {
      this.toastr.error("Mật khẩu và xác nhận mật khẩu chưa trùng nhau","Mật khẩu")
    }
  }

  getListOrderByUserId(userId: any){

  }

  huyDonHang(order: any) {
    let huy = 2;
    this.orderService.chuyenTrangThaiDonHang(order.orderId,huy).subscribe(res => {
      alert(res)
    })
  }
}
