import {Component, OnInit, ViewChild} from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { User } from 'src/app/model/User';
import { UserService } from 'src/app/service/user.service';
import {MatTableDataSource} from "@angular/material/table";
import {AdminOrder} from "../../../admin/order-management/admin-order";

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  user:any
  formGroup: any;
  attributes = {
    name: new FormControl(''),
    phone: new FormControl(''),
    account: new FormControl(''),
    address:new FormControl('')

  };
  constructor(private fb: FormBuilder,private userService: UserService) {
    this.formGroup = this.fb.group(this.attributes);
   }
  userData: any;

  ngOnInit(): void {


  this.userService.currentUserSubject.subscribe(user=>{

    this.user = user
    this.formGroup.patchValue(this.user)
  })
  }

  get FormValue() {
    return this.formGroup.value;
  }
  updateData() {
    let user:User = {...this.FormValue,email: this.FormValue.account,id: this.user.userId}

    this.userService.update(user).subscribe(res=>{

      this.userService.User = user

      alert("Thay đổi thông tin user thành công!")
    })
  }
}


