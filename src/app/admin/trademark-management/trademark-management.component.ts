import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {AdminProduct} from "../product-management/ admin-product";
import {ToastrService} from "ngx-toastr";
import {CommentDTO} from "../../model/CommentDTO";
import {CommentService} from "../../service/comment.service";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-trademark-management',
  templateUrl: './trademark-management.component.html',
  styleUrls: ['./trademark-management.component.css']
})
export class TrademarkManagementComponent implements OnInit {
  displayedColumns: string[] = ["id", "comment", "productId", "userId", "createTime", "status", "action"];
  dataSource = new MatTableDataSource<CommentDTO>();
  btnName: string = "Tạo"


  constructor(
    private toastr: ToastrService,
    private commentService: CommentService,
    public dialog: MatDialog) {
  }

  @ViewChild('paginator') paginator: any;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    this.commentService.getAllProduct().subscribe(res => {
      console.log('ss', res)
      this.dataSource.data = res
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  toggleChangeEvent(event: any, commentId: number) {
    let status = event.checked ? 0 : 1
    if (status == 0) {
      this.commentService.duyetComment(commentId, status).subscribe(res => {
        console.log(res)
        if (res == true) {
          this.toastr.success('Hiển thị bình luận', 'thành công');
          this.ngOnInit()
        } else {
          this.toastr.error('Hiển thị bình luận', 'thất bại');
        }
      });
    } else {
      this.commentService.duyetComment(commentId, status).subscribe(res => {
        console.log(res)
        if (res == true) {
          this.toastr.success('Ẩn bình luận', 'thành công');
          this.ngOnInit()
        } else {
          this.toastr.error('Ẩn bình luận', 'thất bại');
        }
      });
    }
  }

  editComment(element: any) {

  }

  deleteComment(id: number) {
    this.commentService.deleteComment(id).subscribe(res => {
      if(res == null){
        this.toastr.success('Xóa bình luận', 'thành công');
        //window.location.href = "/admin/trademarks"
        this.ngOnInit()
      }
    })
  }

  viewDetails(element: any) {

  }
}
