import { Component, OnInit } from '@angular/core';
import { ChangeDetectionStrategy, } from '@angular/core';
import { UserService } from 'src/app/service/user.service';
import {JwtResponse} from "../../../response/JwtResponse";
import {apiUrl} from "../../../../environments/environment";

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SidebarComponent implements OnInit {

  currentUser: JwtResponse = {};
  username?: string;
  email?: string;
  avatar?: any;
  constructor(
    private userService: UserService
  ) { }

  ngOnInit(): void {
     this.currentUser = this.userService.currentUserValue;
     this.username = this.currentUser?.name;
     this.email = this.currentUser?.account;
     this.avatar = this.currentUser?.avatar;

  }

  logout(){
    alert("ban co chac muon dang xuat")
    this.userService.logout();
  }

}
