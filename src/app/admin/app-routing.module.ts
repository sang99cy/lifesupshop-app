import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UserManagementComponent} from "./user-management/user-management.component";
import {RouterModule, Routes} from "@angular/router";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {MainComponent} from "./layout/main/main.component";
import { CategoryManagementComponent } from './category-management/category-management.component';
import { ProductManagementComponent } from './product-management/product-management.component';
import { OrderManagementComponent } from './order-management/order-management.component';
import {TrademarkManagementComponent} from "./trademark-management/trademark-management.component";
import {StatisticalReportsManagementComponent} from "./statistical-reports-management/statistical-reports-management.component";
import {AuthGuard} from "../_guard/auth.guard";
import { TestComponent } from './test/test.component';
import {AdminProfileComponent} from "./user-management/admin-profile/admin-profile.component";
import {ExportPdfComponent} from "./order-management/export-pdf/export-pdf.component";
import { RoleGuard } from '../_guard/role.guard';
import {AccountResolverService} from "../service/resovle-router";


const routes: Routes = [
  {
    path: 'admin',
    component: MainComponent,
    canActivate: [AuthGuard],
    children : [
      {path: 'dashboard', component: DashboardComponent},
      {path: 'users', component: UserManagementComponent},
      {path: 'categories', component: CategoryManagementComponent},
      {path: 'products', component: ProductManagementComponent},
      {path: 'orders', component: OrderManagementComponent},
      {path: 'trademarks', component: TrademarkManagementComponent},
      {path: 'statistical_reports', component: StatisticalReportsManagementComponent},
      {path: 'profile', component: AdminProfileComponent},
      {
        path: 'test',
        component: TestComponent
      },
      {
        path: 'export',
        component: ExportPdfComponent
      }
    ]
  },

];

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class AppRoutingModule { }
