import { Pipe, PipeTransform } from '@angular/core';
import { apiUrl } from 'src/environments/environment';

@Pipe({
  name: 'image'
})
export class ImagePipe implements PipeTransform {

  transform(value: string) {
    return `${apiUrl}/${value}`
  }

}
