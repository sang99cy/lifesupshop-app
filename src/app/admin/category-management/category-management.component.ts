import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {UserService} from "../../service/user.service";
import {CategoryService} from "../../service/category.service";
import {MatTableDataSource} from "@angular/material/table";
import {AdminProduct} from "../product-management/ admin-product";
import {AdminCategory} from "./admin-category";
import {MatDialog} from "@angular/material/dialog";
import {ProductCreationComponent} from "../product-management/product-creation/product-creation.component";
import {CategoryCreationComponent} from "./category-creation/category-creation.component";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-category-management',
  templateUrl: './category-management.component.html',
  styleUrls: ['./category-management.component.css']
})
export class CategoryManagementComponent implements AfterViewInit, OnInit {

  displayedColumns: string[] = ["categoryId", "categoryName", "categoryType", "status", "createTime", "updateTime", "action"];
  dataSource = new MatTableDataSource<AdminProduct>();
  btnName: string = "Tạo"

  constructor(private categoryService: CategoryService,
              public dialog: MatDialog,
              private toastr: ToastrService
  ) {
  }

  @ViewChild('paginator') paginator: any;

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    this.categoryService.getAll().subscribe(res => {
      this.dataSource.data = res
    })
  }


  openDialog(data: any): void {
    const dialogRef = this.dialog.open(CategoryCreationComponent, {
      width: '500px',
      data: data
    });

    dialogRef.afterClosed().subscribe((result: any) => {
      console.log(result)
      if (result.action == 'add') {
        this.categoryService.create(result.data).subscribe(res => {
          if(res == null){
            this.toastr.success('thêm mới danh mục','thành công')
            this.ngOnInit()
          }else{
            this.toastr.error('thêm mới danh mục','thất bại')
          }
        })
      } else {
        this.categoryService.update(result.data).subscribe(res => {
          console.log(res)
          if(res == null){
            this.toastr.success('cập nhật danh mục','thành công')
            this.ngOnInit()
          }else{
            this.toastr.error('cập nhật danh mục','thất bại')
          }
        })
      }

    });
  }


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  toggleChangeEvent(event: any, categoryId: any) {
    let status = event.checked ? 0 : 1
    this.categoryService.changeStatus(categoryId,status).subscribe(res => {
      console.log(res)
      if(res == true){
        this.toastr.success('câp nhật trạng thái danh mục','thành công');
        this.ngOnInit()
      }else{
        this.toastr.error('cập nhật trạng thái danh mục','thất bại');
      }
    });
  }

  viewDetails(category: AdminCategory) {
    this.openDialog({action: "details", data: category})
  }

  addCategory() {
    let category : AdminCategory = {}
    this.openDialog({
      action: "add",
      data: category
    })
  }

  deleteCategory(categoryId: any) {
    this.categoryService.deleteCategory(categoryId).subscribe(res => {
      console.log("xóa danh mục:" ,res)
      this.ngOnInit();
    })
  }

  editCategory(category: AdminCategory) {
    this.openDialog({action: "edit", data: category})
  }


}
