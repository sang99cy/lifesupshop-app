export interface AdminCategory {
  categoryId?: number;
  categoryName?: string;
  categoryType?: string;
  createTime?: string;
  updateTime?: string;
  status?: number;
}
