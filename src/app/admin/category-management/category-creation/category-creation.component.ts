import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-category-creation',
  templateUrl: './category-creation.component.html',
  styleUrls: ['./category-creation.component.css']
})
export class CategoryCreationComponent implements OnInit {
  map: {id: number; name: string}[] = [{id:0,name:"Áo nam"},{id:1,name:"Quần Nam"},{id:2,name:"Phụ kiện"},{id:3,name:"Dày dép"}];
  btnName: string = "Tạo"
  constructor(  public dialogRef: MatDialogRef<CategoryCreationComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any,) { }

  ngOnInit(): void {
    this.btnName = this.data.action == "add" ? "Tao" : "Sua"
  }

  CreateOrUpdateCategory(){
    this.dialogRef.close({action: this.data.action, data: this.data.data})
  }

}
