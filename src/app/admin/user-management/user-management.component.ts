import {AfterViewInit, Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {UserService} from "../../service/user.service";
import {ToastrService} from "ngx-toastr";
import {AdminUser} from "./admin-user";
import {MatTableDataSource} from "@angular/material/table";
import {ProductCreationComponent} from "../product-management/product-creation/product-creation.component";
import {DetailUserComponent} from "./detail-user/detail-user.component";
import {MatDialog} from "@angular/material/dialog";

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.css']
})
export class UserManagementComponent implements AfterViewInit, OnInit {
  displayedColumns: string[] = ["id", "name", "email", "password", "phone", "role", "active", "action"];
  dataSource = new MatTableDataSource<AdminUser>();

  constructor(
    private toastr: ToastrService,
    private userService: UserService,
    public dialog: MatDialog) {
  }

  @ViewChild('paginator') paginator: any;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit() {
    this.getAllUsers()
  }

  getAllUsers() {
    this.userService.getAllUsers().subscribe(res => {
      console.log(res);
      this.dataSource.data = res;
    });
  }


  toggleChangeEvent($event: any, active: number) {

  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openDialog(data: any): void {
    let dialogRef: any = null;
    if (data.action == 'details') {
      dialogRef = this.dialog.open(DetailUserComponent, {
        width: '1000px',
        height: '800px',
        data: data,
      });
    }
    // dialogRef.afterClosed().subscribe((result: any) => {
    //
    // }
  }

  deleteUser(id: any) {
    this.userService.delete(id).subscribe(res => {
      this.ngOnInit()
      this.toastr.success('xóa thành công', 'tài khoản')
    })
  }

  detailUser(user: AdminUser) {
    this.openDialog({ action: 'details', data: user });
  }
}
