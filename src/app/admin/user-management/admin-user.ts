export interface AdminUser {
  id?: number,
  email?: string,
  password?: string,
  name?: string;
  phone?: string;
  address?: string;
  active?: number;
  role?: string;
  avatar?: string;
}
