import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {OrderStatus} from "../../../enum/OrderStatus";

@Component({
  selector: 'app-approve-order',
  templateUrl: './approve-order.component.html',
  styleUrls: ['./approve-order.component.css']
})
export class ApproveOrderComponent implements OnInit {

  btnName: string = "Tạo"




  map: {id: number; name: string}[] = [{id:0,name:"Chờ xác nhận"},{id:1,name:"Đang xử lý"},{id:2,name:"Hủy đơn hàng"},{id:3,name:"Hoàn thành"}];
  constructor(public dialogRef: MatDialogRef<ApproveOrderComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,) {
    this.btnName = this.data.action == "add" ? "Tao" : "Sua"
  }

  ngOnInit(): void {
    //this.listApproveOrder()
  }


  listApproveOrder(){
    for(var n in OrderStatus) {
      if (typeof OrderStatus[n] === 'number') {
        this.map.push({id: <any>OrderStatus[n], name: n});
      }
    }
    console.log(OrderStatus)
    console.log(this.map);
  }

  approveOrder() {
    this.dialogRef.close({action: this.data.action, data: this.data.data})
  }
}
