import {AfterViewInit, Component, Inject, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {AdminDetailOrder} from "../admin-detail-order";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-detail-order',
  templateUrl: './detail-order.component.html',
  styleUrls: ['./detail-order.component.css']
})
export class DetailOrderComponent implements AfterViewInit,OnInit {
  order:any = {}
  displayedColumns: string[] = ["id", "productName", "productIcon", "productPrice", "count"];
  dataSource = new MatTableDataSource<AdminDetailOrder>();
  constructor(public dialogRef: MatDialogRef<DetailOrderComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {


              }

  ngOnInit(): void {
    this.order = this.data.data.order
    console.log("quang du",this.order)
    this.dataSource.data = this.data.data.productsOrder;
    console.log("quang du",this.dataSource.data)
    // console.log(this.data.data[0].orderId)
  }

  @ViewChild('paginator') paginator: any;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }


}
