export interface AdminOrder {
  orderId?: number,
  buyerEmail?: string,
  buyerName?: string,
  buyerPhone?: string,
  buyerAddress?: string,
  orderAmount?: string,
  orderStatus?: number,
  createTime?: string,
  updateTime?: string,
  userId?: number,
}
