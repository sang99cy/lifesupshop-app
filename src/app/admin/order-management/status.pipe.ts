import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'status'
})
export class StatusPipe implements PipeTransform {

  transform(value: number) {
    switch(value) {
      case 0:
        return "Chờ xác nhận"
      case 1:
        return "Đang xử lí"
      case 2:
        return "Hủy đơn hàng"
      default:
        return "Hoàn thành"
    }
  }

}

