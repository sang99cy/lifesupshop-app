import {Component, Input, OnInit} from '@angular/core';
import { aggregateBy, AggregateDescriptor, AggregateResult } from '@progress/kendo-data-query';
import {InvoiceRow} from "./invoice-row1";

@Component({
  selector: 'app-export-pdf',
  templateUrl: './export-pdf.component.html',
  styleUrls: ['./export-pdf.component.css']
})
export class ExportPdfComponent {

  @Input()
  public data: InvoiceRow[] = [];

  public rightAlign = {
    'text-align': 'right'
  };

  private aggregates: AggregateDescriptor[] = [{
    field: 'qty', aggregate: 'sum'
  }, {
    field: 'total', aggregate: 'sum'
  }];

  public get totals(): AggregateResult {
    return aggregateBy(this.data, this.aggregates) || {};
  }

}
