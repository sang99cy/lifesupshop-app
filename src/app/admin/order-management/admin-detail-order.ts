export interface AdminDetailOrder {
  id?: number,
  orderId?: number,
  productId?: number,
  productName?: string,
  productDescription?: string,
  productIcon?: string,
  categoryType?: number,
  productPrice?: number,
  productStock?: number,
  count?: number
}
