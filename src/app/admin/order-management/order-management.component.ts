import {AfterViewInit, Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {OrderService} from "../../service/order.service";
import {MatTableDataSource} from "@angular/material/table";
import {AdminOrder} from "./admin-order";
import {ToastrService} from "ngx-toastr";
import {MatDialog} from "@angular/material/dialog";
import {DetailOrderComponent} from "./detail-order/detail-order.component";
import {ApproveOrderComponent} from "./approve-order/approve-order.component";
import {jsPDF} from "jspdf";

@Component({
  selector: 'app-order-management',
  templateUrl: './order-management.component.html',
  styleUrls: ['./order-management.component.css']
})
export class OrderManagementComponent implements AfterViewInit, OnInit {

  displayedColumns: string[] = ["orderId", "buyerName", "buyerPhone", "buyerAddress", "createTime", "orderStatus", "orderAmount", "action"];
  dataSource = new MatTableDataSource<AdminOrder>();
  btnName: string = "Tạo"

  constructor(
    private toastr: ToastrService,
    private orderService: OrderService,
    public dialog: MatDialog
  ) {
  }

  @ViewChild('paginator') paginator: any;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit() {
    this.orderService.getAllOrders().subscribe(res => {
      console.log(res);
      this.dataSource.data = res
    })
  }

  openDialog(data: any): void {
    let dialogRef: any = null;
    if (data.action == "details") {
      dialogRef = this.dialog.open(DetailOrderComponent, {
        width: '1000px',
        height: '800px',
        data: data
      });
    }
    if (data.action == "approves") {
      dialogRef = this.dialog.open(ApproveOrderComponent, {
        width: '500px',
        data: data
      });
    }
    dialogRef.afterClosed().subscribe((result: any) => {
      if (result.action == 'approves') {
        this.orderService.chuyenTrangThaiDonHang(result.data.orderId, result.data.orderStatus).subscribe(res => {
          console.log(res)
        })
      }
    })
  }

  toggleChangeEvent($event: any, orderId: number) {

  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  detailOrder(order:any) {

    this.orderService.getDetailOrder(order?.orderId).subscribe(res => {
      console.log(res)
      this.openDialog({action: "details", data: {productsOrder:res,order:order}})
    })
  }

  @ViewChild('content', {static: false}) el!: ElementRef;

  /*xuất báo cáo đơn hàng*/
  exportOrder(orderId: number) {
    const doc = new jsPDF('p','pt','a4');
    (function (jsPDFAPI) {
      var font = 'AAEAAAAQAQAABAAAR......';
      var callAddFont = function () {
        doc.addFileToVFS('Roboto-regular.tff', font);
        doc.addFont('Roboto-regular.tff', 'Roboto-Regular', 'normal');
      };
      jsPDFAPI.events.push(['addFonts', callAddFont])
    })(jsPDF.API);

    console.log(doc.getFontList());
    doc.setFont('Roboto-Regular', 'normal');
    doc.setFontSize(8);
    doc.html(this.el.nativeElement,{
        callback: (pdf) =>{
          pdf.save("order.pdf");
        }
      })

    // let pdf = new jsPDF('p','pt','a4');
    // pdf.setFont("Arimo");
    // pdf.setFontSize(28);
    // pdf.html(this.el.nativeElement,{
    //   callback: (pdf) =>{
    //     pdf.save("order.pdf");
    //   }
    // })
  }

  /*+
  * hủy đơn hàng*/
  deleteOrder(orderId: number) {

  }

  /*duyệt các trạng thái đơn hàng*/
  appoverOrder(order: AdminOrder) {
    this.openDialog({action: "approves", data: order})
  }
}
