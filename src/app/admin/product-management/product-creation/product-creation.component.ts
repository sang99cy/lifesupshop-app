import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, FormGroupDirective, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {CategoryService} from "../../../service/category.service";
import {Category} from "../../../model/Category";
import {ProductService} from "../../../service/product.service";

@Component({
  selector: 'app-product-creation',
  templateUrl: './product-creation.component.html',
  styleUrls: ['./product-creation.component.css']
})
export class ProductCreationComponent implements OnInit {
  file:any
  btnName: string = "Tạo"
  // categories = [
  //   {id: 0, text: "Quần áo"},
  //   {id: 1, text: "Dày dép"},
  //   {id: 2, text: "Mũ"}
  // ]
  categories: Category[] = []

  constructor(
    private categoryService: CategoryService,

    public dialogRef: MatDialogRef<ProductCreationComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.btnName = this.data.action == "add" ? "Tao" : "Sua"

  }

  ngOnInit(): void {
    this.categoryService.getAll().subscribe(result => {
      console.log("categories: ",result)
      this.categories = result;
    })
  }

  CreateOrUpdateProduct() {
    this.dialogRef.close({action: this.data.action, data: {...this.data.data,image:this.file}})
  }

  changeImage(event:any){
    this.file = event.target.files[0]

  }

}
