import {
  AfterViewInit,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';

import { UserService } from '../../service/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { JwtResponse } from '../../response/JwtResponse';
import { Subscription } from 'rxjs';
import { ProductInfo } from '../../model/productInfo';
import { ProductService } from '../../service/product.service';
import { data } from './data';
import { EditSettingsModel, ToolbarItems } from '@syncfusion/ej2-angular-grids';
import { Grid, Edit, Toolbar, Page } from '@syncfusion/ej2-grids';
import { orderData } from './data';
import { MatTableDataSource } from '@angular/material/table';
import { AdminProduct } from './ admin-product';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ProductCreationComponent } from './product-creation/product-creation.component';
import { Product } from 'src/app/model/product';
import { ToastrService } from 'ngx-toastr';

/**
 * Dialog Editing sample
 */

@Component({
  selector: 'app-product-management',
  templateUrl: './product-management.component.html',
  styleUrls: ['./product-management.component.css'],
})
export class ProductManagementComponent implements AfterViewInit, OnInit {
  displayedColumns: string[] = [
    'productId',
    'productName',
    'productPrice',
    'productStock',
    'img',
    'status',
    'createTime',
    'action',
  ];
  dataSource = new MatTableDataSource<AdminProduct>();
  btnName: string = 'Tạo';

  constructor(
    private toastr: ToastrService,
    private productService: ProductService,
    public dialog: MatDialog
  ) {}

  @ViewChild('paginator') paginator: any;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit() {
    this.productService.getAllProduct().subscribe((res) => {
      this.dataSource.data = res;
    });
  }

  getAllProducts() {}

  toggleChangeEvent(event: any, productId: string) {
    let status = event.checked ? 0 : 1;
    this.productService.changeStatus(productId, status).subscribe((res) => {
      console.log('status: ', res);
      if (res == true) {
        this.toastr.success('chuyển trạng thái sản phẩm', 'thành công');
      } else {
        this.toastr.error('chuyển đổi trạng thái sản phẩm', 'thất bại');
      }
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openDialog(data: any): void {
    let dialogRef: any = null;
    if (data.action == 'details') {
      dialogRef = this.dialog.open(ProductCreationComponent, {
        width: '1000px',
        height: '800px',
        data: data,
      });
    } else {
      dialogRef = this.dialog.open(ProductCreationComponent, {
        width: '500px',
        data: data,
      });
    }

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result.data.image != undefined) {
        this.productService
          .uploadImage(result.data.image)
          .subscribe((res: any) => {
            let imageName = res.imageName;
            result.data = { ...result.data, productIcon: imageName };
            delete result.data['image'];
            if (result.action == 'add') {
              this.productService
                .newProductUpload(result.data)
                .subscribe((res) => {
                  this.toastr.success('thêm mới sản phẩm', 'thành công');
                  this.ngOnInit();
                });
            } else {
              //  let bodyUpdate={
              //   productId: this.result.data.productId,
              //   productName: nguyen quang du,
              //   productPrice: 1111,
              //   productStock: 12,
              //   productDescription: quang du,
              //   productIcon: download.jpg,
              //   productStatus: 0,
              //   categoryType: 202,
              //  }
              let bodyUpdate = result.data;

              this.productService
                .updateProductUpload(bodyUpdate)
                .subscribe((res) => {
                  if (res == null) {
                    this.toastr.success('cập nhật sản phẩm', 'thành công');
                    this.ngOnInit();
                  } else {
                    this.toastr.error('cập nhật sản phẩm', 'thất bại');
                  }
                });
            }
          });
      } else {
        if (result.action == 'add') {
          this.productService.newProductUpload(result.data).subscribe((res) => {
            this.toastr.success('thêm mới sản phẩm', 'thành công');
            this.ngOnInit();
          });
        } else {
          //  let bodyUpdate={
          //   productId: this.result.data.productId,
          //   productName: nguyen quang du,
          //   productPrice: 1111,
          //   productStock: 12,
          //   productDescription: quang du,
          //   productIcon: download.jpg,
          //   productStatus: 0,
          //   categoryType: 202,
          //  }
          let bodyUpdate = result.data;

          this.productService
            .updateProductUpload(bodyUpdate)
            .subscribe((res) => {
              if (res == null) {
                this.toastr.success('cập nhật sản phẩm', 'thành công');
                this.ngOnInit();
              } else {
                this.toastr.error('cập nhật sản phẩm', 'thất bại');
              }
            });
        }
      }
    });
  }

  addProduct() {
    let product: AdminProduct = {};

    this.openDialog({
      action: 'add',
      data: product,
    });
  }

  deleteProduct(productId: string) {
    this.productService.deleteProduct1(productId).subscribe((res) => {
      alert('Xoá thành công');
      this.ngOnInit();
    });
  }

  editProduct(product: AdminProduct) {
    this.openDialog({ action: 'edit', data: product });
  }

  viewDetails(product: AdminProduct) {
    this.openDialog({ action: 'details', data: product });
  }
}
