import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NavbarComponent} from "./layout/navbar/navbar.component";
import {SidebarComponent} from "./layout/sidebar/sidebar.component";
import {UserManagementComponent} from "./user-management/user-management.component";
import {AppRoutingModule} from "./app-routing.module";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {
  NbMenuService,
  NbSidebarService,
} from '@nebular/theme';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FooterComponent} from './layout/footer/footer.component';
import {MainComponent} from './layout/main/main.component';
import {ProductManagementComponent} from './product-management/product-management.component';
import {CategoryManagementComponent} from './category-management/category-management.component';
import {OrderManagementComponent} from './order-management/order-management.component';
import {NgxDatatableModule} from "@swimlane/ngx-datatable";
import {TrademarkManagementComponent} from './trademark-management/trademark-management.component';
import {StatisticalReportsManagementComponent} from './statistical-reports-management/statistical-reports-management.component';
import {MatTableModule} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from "@angular/material/input"
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {MatSelectModule} from '@angular/material/select';
import {MatSliderModule} from '@angular/material/slider';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {
  // GridModule,
  EditService,
  ToolbarService,
  SortService,
  PageService,
  GridAllModule
} from '@syncfusion/ej2-angular-grids';
import {DatePickerAllModule} from '@syncfusion/ej2-angular-calendars';
import {NgxPaginationModule} from "ngx-pagination";
import {TestComponent} from './test/test.component';
import {MatButtonModule} from "@angular/material/button";
import {ProductCreationComponent} from './product-management/product-creation/product-creation.component';
import {MAT_DIALOG_DEFAULT_OPTIONS, MatDialogModule} from "@angular/material/dialog";
import {CategoryCreationComponent} from './category-management/category-creation/category-creation.component';
import {AuthGuard} from "../_guard/auth.guard";
import {DetailOrderComponent} from './order-management/detail-order/detail-order.component';
import {ApproveOrderComponent} from './order-management/approve-order/approve-order.component';
import {AdminProfileComponent} from './user-management/admin-profile/admin-profile.component';
import {MatCardModule} from "@angular/material/card";
import {MatGridListModule} from "@angular/material/grid-list";
import {AccumulationChartModule, ChartModule} from "@syncfusion/ej2-angular-charts";
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from "@angular/material/core";
import {UserProfileModule} from "../shared/modules/user-profile.module";
import { CircleChartComponent } from './dashboard/circle-chart/circle-chart.component';
import { ChartAllModule, AccumulationChartAllModule, RangeNavigatorAllModule } from '@syncfusion/ej2-angular-charts';

import { DropDownListAllModule } from '@syncfusion/ej2-angular-dropdowns';

import { DatePickerModule } from '@syncfusion/ej2-angular-calendars';

import { NumericTextBoxModule } from '@syncfusion/ej2-angular-inputs';

import { ButtonModule } from '@syncfusion/ej2-angular-buttons';
import { StatusPipe } from './order-management/status.pipe';
import { LineChartComponent } from './dashboard/line-chart/line-chart.component';
import { ExportPdfComponent } from './order-management/export-pdf/export-pdf.component';
import { InvoiceComponent } from './order-management/export-pdf/invoice.component'

/*import export*/
import { PDFExportModule } from '@progress/kendo-angular-pdf-export';
import {IntlModule} from "@progress/kendo-angular-intl";
import { GridModule } from '@progress/kendo-angular-grid';
import {ButtonsModule} from "@progress/kendo-angular-buttons";
import { ColumnChartComponent } from './dashboard/column-chart/column-chart.component';
import {Image1Pipe} from "./Image1.pipe";
import {NgxPrintModule} from 'ngx-print';
import {NumberPipe} from "./Number.pipe";
import { DetailUserComponent } from './user-management/detail-user/detail-user.component';
@NgModule({
  declarations: [
    NavbarComponent,
    SidebarComponent,
    UserManagementComponent,
    DashboardComponent,
    FooterComponent,
    MainComponent,
    ProductManagementComponent,
    CategoryManagementComponent,
    OrderManagementComponent,
    TrademarkManagementComponent,
    StatisticalReportsManagementComponent,
    TestComponent,
    ProductCreationComponent,
    CategoryCreationComponent,
    DetailOrderComponent,
    ApproveOrderComponent,
    AdminProfileComponent,
    CircleChartComponent,
    StatusPipe,
    LineChartComponent,
    ExportPdfComponent,
    InvoiceComponent,
    ColumnChartComponent,
    Image1Pipe,
    NumberPipe,
    DetailUserComponent
  ],
  imports: [
    NgxPrintModule,
    CommonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    NgxPaginationModule,
    AppRoutingModule,
    UserProfileModule,
    RouterModule, HttpClientModule,
    BrowserAnimationsModule, NgxDatatableModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatSliderModule,
    GridModule,
    GridAllModule,
    DatePickerAllModule,
    MatPaginatorModule,
    MatSlideToggleModule,
    MatButtonModule,
    MatDialogModule,
    MatSelectModule,
    ReactiveFormsModule, MatCardModule, MatGridListModule,
    ChartModule,
    AccumulationChartModule,
    ChartAllModule, RangeNavigatorAllModule, BrowserModule, ButtonModule,
    AccumulationChartAllModule, NumericTextBoxModule, DatePickerModule, DropDownListAllModule,
    /*ss*/
    IntlModule,
    GridModule,
    PDFExportModule,
    ButtonsModule
  ],
  providers: [MatDatepickerModule,NbMenuService, NbSidebarService,
    EditService, ToolbarService, SortService, PageService, AuthGuard,

    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: true}}
  ],

})
export class ModuleadminModule {
}
