import {Component, OnInit} from '@angular/core';
import {
  CategoryService, DateTimeService, ScrollBarService, ColumnSeriesService, LineSeriesService,
  ChartAnnotationService, RangeColumnSeriesService, StackingColumnSeriesService, LegendService, TooltipService
} from '@syncfusion/ej2-angular-charts';
import {StatisticsService} from "../../service/statistics.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [CategoryService, DateTimeService, ScrollBarService, LineSeriesService, ColumnSeriesService,
    ChartAnnotationService, RangeColumnSeriesService, StackingColumnSeriesService, LegendService, TooltipService,]
})
export class DashboardComponent implements OnInit {
  dateFrom: any
  dateTo: any
  soLuongDonHangHomNay: number = 0;
  soLuongDonHangThangNay: number = 0;
  doanhThuNgayHomNay: number = 0;
  doanhthuThangNay: number = 0;

  public primaryXAxis: Object = {};
  public chartData: Object[] = [{}];

  ngOnInit(): void {
    /*this.chartData = [
      {month: 'Jan', sales: 35}, {month: 'Feb', sales: 28},
      {month: 'Mar', sales: 34}, {month: 'Apr', sales: 32},
      {month: 'May', sales: 40}, {month: 'Jun', sales: 32},
      {month: 'Jul', sales: 35}, {month: 'Aug', sales: 55},
      {month: 'Sep', sales: 38}, {month: 'Oct', sales: 30},
      {month: 'Nov', sales: 25}, {month: 'Dec', sales: 32}
    ];*/
    this.primaryXAxis = {
      valueType: 'Category'
    };
    this.Filter();
    this.thongkeSoluongDonHangHomNay();
    this.thongkeSoluongDonHangThangNay();
    this.thongkeDoanhThuThangNay();
    this.thongkeDoanhThuNgayHomNay()
  }

  constructor(private statisticsService: StatisticsService) {
  }


  format(date: any) {
    let new_arr = []
    let date_str = new Date(date).toLocaleDateString()
    let date_lis = date_str.split("/")
    for (let i of date_lis) {
      i = i.length == 1 ? `0${i}` : i
      new_arr.push(i)
    }
    return `${new_arr[2]}-${new_arr[0]}-${new_arr[1]}`

  }

  Filter() {
    let date_from_str = this.format(this.dateFrom)
    let date_to_str = this.format(this.dateTo)
    //console.log(date_from_str, date_to_str)
    const time= {
      dateFrom: date_from_str,
      dateTo: date_to_str
    }
    this.statisticsService.thongkeDoanhThuHangNgay(time).subscribe(res=>{
      //console.log(res)
      this.chartData = res;
    })
  }

  thongkeSoluongDonHangHomNay(){
    this.statisticsService.thongkeSoluongDonHangHomnay().subscribe(res => {
      this.soLuongDonHangHomNay = res.soluong
    })
  }
  thongkeSoluongDonHangThangNay(){
    this.statisticsService.thongkeSoluongDonHangThangnay().subscribe(res => {
      this.soLuongDonHangThangNay= res.soluong
    })
  }

  thongkeDoanhThuNgayHomNay(){
    this.statisticsService.thongkeDoanhThuHomnay().subscribe(res=> {
      console.log('doanh thu ngay hom nay', res)
      this.doanhThuNgayHomNay =  res.doanhThu
    })
  }

  thongkeDoanhThuThangNay(){
    this.statisticsService.thongkeDoanhThuThangnay().subscribe(res => {
      console.log(res)
      this.doanhthuThangNay = res.doanhThu
    })
  }

}
