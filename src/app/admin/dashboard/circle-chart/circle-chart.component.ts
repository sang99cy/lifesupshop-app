import {Component, ViewChild, ViewEncapsulation, OnInit} from '@angular/core';
import {
  AccumulationChartComponent, IAccTextRenderEventArgs, AccumulationChart, GroupModes,
  IAccPointRenderEventArgs, IAccLoadedEventArgs, AccumulationTheme
} from '@syncfusion/ej2-angular-charts';
import {DropDownList} from '@syncfusion/ej2-dropdowns';
import {StatisticsService} from "../../../service/statistics.service";

@Component({
  selector: 'app-circle-chart',
  templateUrl: './circle-chart.component.html',
  styleUrls: ['./circle-chart.component.css']
})
export class CircleChartComponent implements OnInit {


  public data: Object[] = [
     /*{'x': 'China', y: 26, text: 'China: 26'},
     {'x': 'Russia', y: 19, text: 'Russia: 19'},
    {'x': 'Germany', y: 17, text: 'Germany: 17'},
    {'x': 'Japan', y: 12, text: 'Japan: 12'},*/

  ];
  @ViewChild('pie')
  public pie: any

  public onTextRender(args: IAccTextRenderEventArgs): void {

    args.text ='Quý '+ args.point.x + ' : ' + args.point.y;
  }

  public onPointRender(args: IAccPointRenderEventArgs): void {
    if (args.point.isClubbed || args.point.isSliced) {
      args.fill = '#D3D3D3';
    }
  }

  public explode: boolean = true;
  //Initializing Legend
  public legendSettings: Object = {
    visible: false,
  };
  //Initializing DataLabel
  public dataLabel: Object = {
    visible: true,
    position: 'Outside',
    connectorStyle: {type: 'Line', length: '5%'},
    font: {
      size: '14px'
    }
  };

  public onChange(e: Event): void {
    let element: HTMLInputElement = <HTMLInputElement>e.target;
    let currentValue: number = element.value === 'Point' ? 9 : 8;
    this.pie.series[0].groupMode = <GroupModes>element.value;
    this.pie.series[0].groupTo = currentValue.toString();
    this.pie.series[0].animation.enable = false;
    (document.getElementById('clubtext') as HTMLInputElement).innerHTML = currentValue.toString();
    this.pie.removeSvg();
    this.pie.refreshSeries();
    this.pie.refreshChart();
  };

  public onClubvalue(e: Event): void {
    let clubvalue: string = (document.getElementById('clubvalue') as HTMLSelectElement).value;
    this.pie.series[0].groupTo = clubvalue;
    this.pie.series[0].animation.enable = false;
    (document.getElementById('clubtext') as HTMLInputElement).innerHTML = clubvalue;
    this.pie.removeSvg();
    this.pie.refreshSeries();
    this.pie.refreshChart();
  };

  // custom code start
  public load(args: IAccLoadedEventArgs): void {
    let selectedTheme: string = location.hash.split('/')[1];
    selectedTheme = selectedTheme ? selectedTheme : 'Material';
    args.accumulation.theme = <AccumulationTheme>(selectedTheme.charAt(0).toUpperCase() + selectedTheme.slice(1)).replace(/-dark/i, "Dark");
  };

  // custom code end
  public clubvalue: string = '9';
  public startAngle: number = 0;
  public endAngle: number = 360;
  public tooltip: Object = {enable: false};
  public title: string = 'Thống kê doanh thu theo quý';
  public groupMode: any;

  ngOnInit(): void {
    this.groupMode = new DropDownList({
      index: 0,
      width: 120,
      change: () => {
        let mode: string = this.groupMode.value.toString();
        let currentValue: number = mode === 'Point' ? 9 : 8;
        this.pie.series[0].groupMode = <GroupModes>mode;
        this.pie.series[0].groupTo = currentValue.toString();
        this.pie.series[0].animation.enable = false;
        (document.getElementById('clubtext') as HTMLInputElement).innerHTML = currentValue.toString();
        (document.getElementById('clubvalue') as HTMLInputElement).value = currentValue.toString();
        this.pie.removeSvg();
        this.pie.refreshSeries();
        this.pie.refreshChart();
      }
    });
    this.groupMode.appendTo('#mode');
   this.thongKeDoanhThuOrdonHangTheoQuy(0);
  }

  constructor(private statisticsService: StatisticsService) {
  };

  thongKeDoanhThuOrdonHangTheoQuy(type: number) {
    this.statisticsService.thongkeDoanhThuOrSoluongTheoQuy(type).subscribe(res => {

      this.data =  this.parseToDataSourse(res)
    })
  }

  parseToDataSourse(originData:any){
    let new_arr = []
    for(let data of originData){
      let item = {
        x:data.quy,
        y:data.doanhThu 
      }
      new_arr.push(item)
    }
    return new_arr
  }
}


