import { Component, OnInit } from '@angular/core';
import {StatisticsService} from "../../../service/statistics.service";
import { ILoadedEventArgs, ChartTheme } from '@syncfusion/ej2-angular-charts';
import { Browser } from '@syncfusion/ej2-base';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})
export class LineChartComponent implements OnInit {
  public data: Object[] = [

  ];

  dateFrom: any
  dateTo: any

  format(date: any) {
    let new_arr = []
    let date_str = new Date(date).toLocaleDateString()
    let date_lis = date_str.split("/")
    for (let i of date_lis) {
      i = i.length == 1 ? `0${i}` : i
      new_arr.push(i)
    }
    return `${new_arr[2]}-${new_arr[0]}-${new_arr[1]}`

  }


  Filter() {
    let date_from_str = this.format(this.dateFrom)
    let date_to_str = this.format(this.dateTo)
    //console.log(date_from_str, date_to_str)
    const time= {
      dateFrom: date_from_str,
      dateTo: date_to_str
    }
    this.statisticsService.thongkeDoanhThuHangNgay(time).subscribe(res=>{

      this.data = this.parseDataSourse(res);
      console.log(this.data)
    })
  }

  constructor(private statisticsService: StatisticsService) { }

  ngOnInit(): void {
    this.Filter()
  }

  //Initializing Primary X Axis
  public primaryXAxis: Object = {
    valueType: 'DateTime',
    labelFormat: 'dd/MM/yy',

    edgeLabelPlacement: 'Shift',
    majorGridLines: { width: 0 }
  };
  //Initializing Primary Y Axis
  public primaryYAxis: Object = {
    labelFormat: '{value} VNĐ',
    rangePadding: 'None',
    minimum: 0,
    maximum: 700,
    interval:100 ,
    lineStyle: { width: 5 },
    majorTickLines: { width: 5 },
    minorTickLines: { width: 5 }
  };
  public chartArea: Object = {
    border: {
      width: 0
    }
  };
  public width: string = Browser.isDevice ? '100%' : '60%';
  public marker: Object = {
    visible: true,
    height: 10,
    width: 10
  };
  public tooltip: Object = {
    enable: true
  };
  // custom code start
  public load(args: ILoadedEventArgs): void {
    let selectedTheme: string = location.hash.split('/')[1];
    selectedTheme = selectedTheme ? selectedTheme : 'Material';
    args.chart.theme = <ChartTheme>(selectedTheme.charAt(0).toUpperCase() + selectedTheme.slice(1)).replace(/-dark/i, "Dark");
  };
  // custom code end
  public title: string = 'Doanh số hàng ngày';

  parseDataSourse(originDataSourse:any){
    let new_arr = []
    for(let item of originDataSourse){
      let new_item = {
        x:new Date(item.ngay),
        y:item.doanhThu / 1000
      }
      new_arr.push(new_item)
    }
    return new_arr
  }
}
