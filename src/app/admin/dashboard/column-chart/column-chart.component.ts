import { Component, OnInit } from '@angular/core';
import { StatisticsService } from 'src/app/service/statistics.service';

@Component({
  selector: 'app-column-chart',
  templateUrl: './column-chart.component.html',
  styleUrls: ['./column-chart.component.css'],
})
export class ColumnChartComponent implements OnInit {
  public primaryXAxis: Object = {};
  public chartData: Object[] = [];

  createForm: any = null;
  createTo: any = null;

  format(date: any) {
    let new_arr = [];
    let date_str = new Date(date).toLocaleDateString();
    let date_lis = date_str.split('/');
    for (let i of date_lis) {
      i = i.length == 1 ? `0${i}` : i;
      new_arr.push(i);
    }
    return `${new_arr[2]}-${new_arr[0]}-${new_arr[1]}`;
  }

  Filter() {
    let date_from_str = this.createForm ? this.format(this.createForm):null;
    let date_to_str = this.createTo ? this.format(this.createTo) : null;
    console.log(date_from_str, date_to_str)
    const time = {
      createForm: date_from_str,
      createTo: date_to_str,
    };
    console.log('time',time)
    this.statisticsService.thongkeDoanhThuHangNgay(time).subscribe((res) => {
      console.log("rea",res)
      this.chartData = res
    })

  }

  constructor(private statisticsService: StatisticsService) {}

  ngOnInit(): void {
    this.Filter();
    this.chartData = [
      // { month: 'Jan', sales: 35 },
      // { month: 'Feb', sales: 28 },
      // { month: 'Mar', sales: 34 },
      // { month: 'Apr', sales: 32 },
      // { month: 'May', sales: 40 },
      // { month: 'Jun', sales: 32 },
      // { month: 'Jul', sales: 35 },
      // { month: 'Aug', sales: 55 },
      // { month: 'Sep', sales: 38 },
      // { month: 'Oct', sales: 30 },
      // { month: 'Nov', sales: 25 },
      // { month: 'Dec', sales: 32 },
    ];
    this.primaryXAxis = {
      valueType: 'Category',
    };
  }
}
