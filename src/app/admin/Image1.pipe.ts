import { Pipe, PipeTransform } from '@angular/core';
import { apiUrl } from 'src/environments/environment';

@Pipe({
  name: 'image1'
})
export class Image1Pipe implements PipeTransform {

  transform(value: string) {
    return `${apiUrl}/${value}`
  }

}
