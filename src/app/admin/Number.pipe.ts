import { Pipe, PipeTransform } from '@angular/core';
import { apiUrl } from 'src/environments/environment';

@Pipe({
  name: 'money'
})
export class NumberPipe implements PipeTransform {

  transform(value: number) {
    return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }

}
