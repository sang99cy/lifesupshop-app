import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, Route, Router } from '@angular/router';
import { UserService } from '../service/user.service';

@Injectable({providedIn: 'root'})
export class RoleGuard implements CanActivate {
  constructor(private userService: UserService,private router:Router) { }

  canActivate(){
    if(this.userService.User.role != 'ROLE_MANAGER'){
      this.router.navigate(['/login'])
      return false
    }
    return true
  }
}
