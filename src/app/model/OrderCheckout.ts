import {ProductInOrder} from "./ProductInOrder";

export class OrderCheckout {
  orderId?: number;
  buyerEmail?: string;
  buyerName?: string;
  buyerPhone?: string;
  buyerAddress?: string;
  orderAmount?: number;
  orderStatus?: number;
  createTime?: string;
  updateTime?: string;
  userId?: number;
  payment?: number;
  comment?: string;
  detailOrders?: Array<ProductInOrder>;
}
