export interface CommentDTO {
  id: number,
  name: string,
  comment: string,
  productId: string,
  userId: number,
  createTime: Date,
  status: number,
  username: string,
  avatar: string
}
