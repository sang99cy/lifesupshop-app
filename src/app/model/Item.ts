import {ProductInfo} from "./productInfo";
import { ProductInOrder } from "./ProductInOrder";

export interface Item {
    quantity: number;
    productInfo: ProductInOrder

}
